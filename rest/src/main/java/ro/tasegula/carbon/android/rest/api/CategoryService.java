/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface CategoryService {

    @GET(URLService.CATEGORY)
    Single<List<SetWrapper>> getCategories();

    @GET(URLService.CATEGORY_ID)
    Single<SetWrapper> getCategory();

    @GET(URLService.CATEGORY_SHOW)
    Single<List<SuiteWrapper>> getSuitesForId(@Path("id") int id);

    @GET(URLService.CATEGORY_SHOW)
    Single<List<SuiteWrapper>> getMaxSuitesForId(@Path("id") int id,
            @Query("max") int max);

    /**
     * Factory class to get a singleton instance of {@link CategoryService}
     */
    class Client extends AbstractClient<CategoryService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(CategoryService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
