/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

import ro.tasegula.carbon.android.rest.utils.stats.StatsData;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class StatsQuestionWrapper {
	private long statsId;

	private long questionId;
	private String question;

	private long suiteId;
	private String suiteName;

	private String imageUri;

	private StatsData data;

	private List<StatsAnswerWrapper> stats = new ArrayList<>();

	private Timestamp createdAt;

	public long getStatsId() {
		return statsId;
	}

	public void setStatsId(long statsId) {
		this.statsId = statsId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public long getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(long suiteId) {
		this.suiteId = suiteId;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public StatsData getData() {
		return data;
	}

	public void setData(StatsData data) {
		this.data = data;
	}

	public List<StatsAnswerWrapper> getStats() {
		return stats;
	}

	public void setStats(List<StatsAnswerWrapper> stats) {
		this.stats = stats;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
}
