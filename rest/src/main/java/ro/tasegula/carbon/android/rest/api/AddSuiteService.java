/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface AddSuiteService {

    @POST(URLService.SUITE)
    Single<SuiteWrapper> addSuite(@Body SuiteWrapper question,
            @Query("userId") int user);

    @Multipart
    @POST(URLService.SUITE_IMAGE)
    Single<SuiteWrapper> addSuiteImage(@Part("body") SuiteWrapper question,
            @Part MultipartBody.Part file,
            @Query("userId") int userId);

    /**
     * Factory class to get a singleton instance of {@link AddSuiteService}
     */
    class Client extends AbstractClient<AddSuiteService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(AddSuiteService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
