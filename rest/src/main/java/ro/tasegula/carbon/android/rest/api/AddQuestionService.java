/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface AddQuestionService {

    @POST(URLService.QUESTION)
    Single<QuestionWrapper> addQuestion(@Body QuestionWrapper question);

    @Multipart
    @POST(URLService.QUESTION_IMAGE)
    Single<QuestionWrapper> addQuestionWithImage(@Part("body") QuestionWrapper question,
            @Part MultipartBody.Part file,
            @Query("userId") int userId);

    /**
     * Factory class to get a singleton instance of {@link AddQuestionService}
     */
    class Client extends AbstractClient<AddQuestionService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(AddQuestionService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
