/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.android.rest.model;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused")
public class StatsPostWrapper {

	private long userId;
	private long questionId;
	private int rating;
	private boolean correct;
	private List<String> answers;
	private Timestamp answeredAt;

	public StatsPostWrapper() {}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public Timestamp getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(Timestamp answeredAt) {
		this.answeredAt = answeredAt;
	}

	@Override
	public String toString() {
		return "StatsPostWrapper{" +
		       "userId=" + userId +
		       ", questionId=" + questionId +
		       ", correct=" + correct +
		       ", rating=" + rating +
		       ", answers=" + answers +
		       '}';
	}
}
