/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

@SuppressWarnings("unused")
public class UserWrapper extends AbstractJsonModel {

	private int id;
	private String googleId;
	private String googleName;
	private String googleEmail;
	private String googleImage;

	public UserWrapper() {}

	public UserWrapper(int id, String googleId, String googleName, String googleEmail, String googleImage) {
		this.id = id;
		this.googleId    = googleId;
		this.googleName  = googleName;
		this.googleEmail = googleEmail;
		this.googleImage = googleImage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getGoogleName() {
		return googleName;
	}

	public void setGoogleName(String googleName) {
		this.googleName = googleName;
	}

	public String getGoogleEmail() {
		return googleEmail;
	}

	public void setGoogleEmail(String googleEmail) {
		this.googleEmail = googleEmail;
	}

	public String getGoogleImage() {
		return googleImage;
	}

	public void setGoogleImage(String googleImage) {
		this.googleImage = googleImage;
	}

	@Override
	public String toString() {
		return "UserWrapper{" +
				"id=" + id +
				", googleId='" + googleId + '\'' +
				", googleName='" + googleName + '\'' +
				", googleEmail='" + googleEmail + '\'' +
				", googleImage='" + googleImage + '\'' +
				'}';
	}
}
