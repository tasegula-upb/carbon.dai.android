/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

@SuppressWarnings("unused")
public class SetWrapper extends AbstractJsonModel {

	private long id;
	private String name;

	private int noSuites;

	public SetWrapper(long id, String name, int noSuites) {
		this.id = id;
		this.name = name;
		this.noSuites = noSuites;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoSuites() {
		return noSuites;
	}

	public void setNoSuites(int noSuites) {
		this.noSuites = noSuites;
	}

	@Override
	public String toString() {
		return "CategoryWrapper{" +
				"id=" + id +
				", name='" + name + '\'' +
				", noSuites=" + noSuites +
				'}';
	}
}
