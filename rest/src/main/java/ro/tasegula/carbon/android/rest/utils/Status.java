/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.utils;

@SuppressWarnings("unused")
public class Status {

	private int code;
	private String message;

	public Status(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SearchResultEntity{" +
				"code='" + code + '\'' +
				", message='" + message + '\'' +
				'}';
	}

	// -------------------------------------------------------------------------
	// BASIC STATIC OBJECTS
	// -------------------------------------------------------------------------

	public static final Status SUCCESS = new Status(1, "SUCCESS");
	public static final Status ERROR   = new Status(0, "ERROR");
}
