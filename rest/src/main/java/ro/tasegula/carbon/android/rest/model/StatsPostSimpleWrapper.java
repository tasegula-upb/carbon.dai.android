/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.android.rest.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class StatsPostSimpleWrapper {

	private long questionId;
	private boolean correct;
	private int rating;
	private List<String> answers = new ArrayList<>();
	private Timestamp answeredAt;

	public StatsPostSimpleWrapper() {}

	public static StatsPostSimpleWrapper newStats() {
		StatsPostSimpleWrapper stats = new StatsPostSimpleWrapper();
		stats.setQuestionId(0);
		stats.setRating(0);
		stats.setCorrect(false);

		Date date = new Date();
		stats.setAnsweredAt(new Timestamp(date.getTime()));

		return stats;
	}

	public boolean isFilled() {
		return answers.size() > 0 || questionId > 0;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public void addAnswer(String answer) {
		this.answers.add(answer);
	}

	public Timestamp getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(Timestamp answeredAt) {
		this.answeredAt = answeredAt;
	}

	@Override
	public String toString() {
		return "StatsPostSimpleWrapper{" +
				"questionId=" + questionId +
				", correct=" + correct +
				", rating=" + rating +
				", answers=" + answers +
				", answeredAt=" + answeredAt +
				'}';
	}
}
