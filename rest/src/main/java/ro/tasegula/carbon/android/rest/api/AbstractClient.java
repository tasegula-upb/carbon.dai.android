package ro.tasegula.carbon.android.rest.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ro.tasegula.carbon.android.rest.utils.JsonUtils;
import ro.tasegula.carbon.android.rest.utils.URLService;

public class AbstractClient<T> {
    private static Retrofit sRetrofit;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client =
                new OkHttpClient.Builder()
                        .connectTimeout(3000, TimeUnit.MILLISECONDS)
                        .readTimeout(3000, TimeUnit.MILLISECONDS)
                        .writeTimeout(3000, TimeUnit.MILLISECONDS)
                        .addInterceptor(interceptor)
                        .build();

        sRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(JsonUtils.getGsonForTimestamp()))
                .baseUrl(URLService.API_URI)
                .client(client)
                .build();
    }

    T api;

    public T getAPI() {
        return api;
    }

    protected Retrofit getRetrofit() {
        return sRetrofit;
    }
}
