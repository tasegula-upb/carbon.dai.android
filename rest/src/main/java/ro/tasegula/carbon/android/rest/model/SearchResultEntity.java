/**
 * Project Carbon: created by Tase Gula.
 *
 * This file is part of the project's Android App.
 */
package ro.tasegula.carbon.android.rest.model;

@SuppressWarnings("unused")
public class SearchResultEntity {

	public String id;
	public String title;
	public String description;

	public SearchResultEntity(String id, String title, String description) {
		this.id = id;
		this.title = title;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "SearchResultEntity{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", description='" + description + '\'' +
				'}';
	}
}
