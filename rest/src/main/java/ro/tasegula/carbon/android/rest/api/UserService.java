/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.rest.model.UserWrapper;
import ro.tasegula.carbon.android.rest.utils.Status;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface UserService {

    @GET(URLService.USER_ID)
    Single<UserWrapper> getUser();

    @GET(URLService.USER_SHOW)
    Single<List<SetWrapper>> getCollectionsForId(@Path("id") int id);

    @GET(URLService.USER_SUITES)
    Single<List<SuiteWrapper>> getSuitesForId(@Path("id") int id);

    @GET(URLService.USER_SUITES_EDITABLE)
    Single<List<SuiteWrapper>> getEditableSuitesForId(@Path("id") int id);

    // ------------------------------------------------------------------------
    // POST for CREATE-USER
    // ------------------------------------------------------------------------

    @POST(URLService.USER)
    Single<UserWrapper> postUser(@Body UserWrapper user);

    // ------------------------------------------------------------------------
    // POST for copy SUITE
    // ------------------------------------------------------------------------

    @POST(URLService.USER_ID)
    Single<Status> postCopySuite(@Path("id") int id,
            @Body long suiteId);

    /**
     * Factory class to get a singleton instance of {@link UserService}
     */
    class Client extends AbstractClient<UserService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(UserService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
