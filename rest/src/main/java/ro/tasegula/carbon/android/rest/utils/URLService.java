/**
 * Project Carbon: created by Tase Gula.
 *
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.utils;

@SuppressWarnings("unused")
public class URLService {

	public static final String LOCALHOST1 = "http://10.0.3.2:8080";
	public static final String LOCALHOST2 = "http://192.168.100.3:8080";
	public static final String AWS = "http://ec2-54-93-80-2.eu-central-1.compute.amazonaws.com:8080/server";
	public static final String API_URI = LOCALHOST2;

	// URL for file upload
	public static final String UPLOAD = API_URI + "/upload";

	// URL for images
	public static final String IMAGE_BASE_URL = API_URI + "/images";
	public static String imagesUrl(String partialUri) {
		return IMAGE_BASE_URL + partialUri;
	}

	// DRAWER
	public static final String DISCOVER = "/discover";

	// CATEGORY
	public static final String CATEGORY      = "/category";
	public static final String CATEGORY_ID   = CATEGORY    + "/{id}";
	public static final String CATEGORY_SHOW = CATEGORY_ID + "/show";

	// SUITE
	public static final String SUITE           = "/suite";
	public static final String SUITE_IMAGE     = SUITE + "/image";
	public static final String SUITE_ADD       = SUITE      + "/add";
	public static final String SUITE_ID        = SUITE      + "/{id}";
	public static final String SUITE_SHOW      = SUITE_ID   + "/show";
	public static final String SUITE_SHOW_LESS = SUITE_SHOW + "/less";

	// QUESTION
	public static final String QUESTION         = "/question";
	public static final String QUESTION_IMAGE   = QUESTION + "/image";
	public static final String QUESTION_ID      = QUESTION + "/{id}";

	// COLLECTION
	public static final String COLLECTION           = "/collection";
	public static final String COLLECTION_ID        = COLLECTION      + "/{id}";
	public static final String COLLECTION_SHOW      = COLLECTION_ID   + "/show";
	public static final String COLLECTION_SHOW_LESS = COLLECTION_SHOW + "/less";

	// USER
	public static final String USER        = "/user";
	public static final String USER_ADD    = USER    + "/add";
	public static final String USER_ID     = USER    + "/{id}";
	public static final String USER_SHOW   = USER_ID + "/show";
	public static final String USER_SUITES = USER_ID + "/show/all";
	public static final String USER_SUITES_EDITABLE = USER_SUITES + "/private";

	// USER STATS
	public static final String STATS           = "/stats/user";
	public static final String STATS_USER      = STATS    + "/{id}";
	public static final String STATS_USER_LESS = STATS_USER    + "/less";
	public static final String STATS_USER_DB   = STATS_USER + "/db";
}
