/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class StatsWrapper<T> {

	private long userId;
	private List<T> stats = new ArrayList<>();

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<T> getStats() {
		return stats;
	}

	public void setStats(List<T> stats) {
		this.stats = stats;
	}
}
