/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface QuestionService {

    @GET(URLService.QUESTION_ID)
    Single<QuestionWrapper> getQuestion(@Path("id") int id);

    /**
     * Factory class to get a singleton instance of {@link QuestionService}
     */
    class Client extends AbstractClient<QuestionService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(QuestionService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
