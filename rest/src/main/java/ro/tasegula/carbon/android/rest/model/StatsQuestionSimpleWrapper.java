/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

import ro.tasegula.carbon.android.rest.utils.stats.StatsData;

@SuppressWarnings("unused")
public class StatsQuestionSimpleWrapper {
	private long statsId;

	private long suiteId;
	private String suiteName;

	private long questionId;
	private String question;

	private String answer;

	private String imageUri;

	private StatsData data;

	public long getStatsId() {
		return statsId;
	}

	public void setStatsId(long id) {
		this.statsId = id;
	}

	public long getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(long suiteId) {
		this.suiteId = suiteId;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public StatsData getData() {
		return data;
	}

	public void setData(StatsData data) {
		this.data = data;
	}
}
