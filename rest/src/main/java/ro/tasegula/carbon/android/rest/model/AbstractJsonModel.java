/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("unused")
abstract class AbstractJsonModel {

	public String printJson() {
		return new Gson().toJson(this);
	}

	public String printPrettyJson() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);
	}
}
