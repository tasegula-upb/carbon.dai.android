/**
 * /**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface DiscoverService {

    @GET(URLService.DISCOVER)
    Single<List<SetWrapper>> getAll();

    /**
     * Factory class to get a singleton instance of {@link DiscoverService}
     */
    class Client extends AbstractClient<DiscoverService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(DiscoverService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
