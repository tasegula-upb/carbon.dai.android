/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
public class QuestionWrapper extends AbstractJsonModel {

	private long id;

	private String question;
	private String hint;
	private List<AnswerWrapper> answers;

	private Timestamp createdAt;

	private long suiteId;
	private String imageUri;

	public QuestionWrapper() {}

	public QuestionWrapper(long id, String question, String hint, List<AnswerWrapper> answers, Timestamp createdAt, long suiteId, String imageUri) {
		this.id = id;
		this.question = question;
		this.hint = hint;
		this.answers = answers;
		this.createdAt = createdAt;
		this.suiteId = suiteId;
		this.imageUri = imageUri;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public List<AnswerWrapper> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerWrapper> answers) {
		this.answers = answers;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public long getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(long suiteId) {
		this.suiteId = suiteId;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	@Override
	public String toString() {
		return "QuestionWrapper{" +
				"id=" + id +
				", question='" + question + '\'' +
				", answers=" + ((answers == null) ? null : Arrays.toString(answers.toArray())) +
				", createdAt=" + createdAt +
				", suiteId=" + suiteId +
				", imageUri='" + imageUri + '\'' +
				'}';
	}
}
