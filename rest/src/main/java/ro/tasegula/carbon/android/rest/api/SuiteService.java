/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface SuiteService {

    @GET(URLService.SUITE_ID)
    Single<SuiteWrapper> getSuite();

    @GET(URLService.SUITE_SHOW)
    Single<List<QuestionWrapper>> getQuestionsForId(@Path("id") int id);

    @GET(URLService.SUITE_SHOW_LESS)
    Single<List<Integer>> getQuestionIdsForId(@Path("id") int id);

    @GET(URLService.SUITE_SHOW)
    Single<List<QuestionWrapper>> getMaxQuestionsForId(@Path("id") int id,
            @Query("max") int max);

    @GET(URLService.SUITE_SHOW)
    Single<List<QuestionWrapper>> getPageQuestionsForId(@Path("id") int id,
            @Query("page") int page);

    /**
     * Factory class to get a singleton instance of {@link SuiteService}
     */
    class Client extends AbstractClient<SuiteService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(SuiteService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
