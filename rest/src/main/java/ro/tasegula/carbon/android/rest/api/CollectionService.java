/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface CollectionService {

    @GET(URLService.COLLECTION_ID)
    Single<SetWrapper> getCollection();

    @GET(URLService.COLLECTION_SHOW)
    Single<List<SuiteWrapper>> getSuitesForId(@Path("id") int id);

    @GET(URLService.COLLECTION_SHOW_LESS)
    Single<List<Integer>> getSuitesIdsForId(@Path("id") int id);

    /**
     * Factory class to get a singleton instance of {@link CollectionService}
     */
    class Client extends AbstractClient<CollectionService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(CollectionService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
