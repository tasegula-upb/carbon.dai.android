package ro.tasegula.carbon.android.rest.api;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ro.tasegula.carbon.android.rest.model.DatabaseStatsWrapper;
import ro.tasegula.carbon.android.rest.model.StatsPostSimpleWrapper;
import ro.tasegula.carbon.android.rest.model.StatsQuestionSimpleWrapper;
import ro.tasegula.carbon.android.rest.model.StatsQuestionWrapper;
import ro.tasegula.carbon.android.rest.model.StatsWrapper;
import ro.tasegula.carbon.android.rest.utils.URLService;

public interface StatsService {

    @GET(URLService.STATS_USER_DB)
    Single<DatabaseStatsWrapper> getUserDbStats(@Path("id") int userId);

    @GET(URLService.STATS_USER)
    Single<StatsWrapper<StatsQuestionWrapper>> getUserStats(@Path("id") int userId);

    @GET(URLService.STATS_USER)
    Single<StatsWrapper<StatsQuestionWrapper>> getUserStatsByPage(@Path("id") int userId,
            @Query("page") int page);

    @GET(URLService.STATS_USER)
    Single<StatsWrapper<StatsQuestionWrapper>> getUserStatsForSuite(@Path("id") int userId,
            @Query("suite") int suiteId);

    @GET(URLService.STATS_USER_LESS)
    Single<StatsWrapper<StatsQuestionSimpleWrapper>> getUserStatsLessForSuite(@Path("id") int userId,
            @Query("suite") int suiteId);

    @GET(URLService.STATS_USER_LESS)
    Single<StatsWrapper<StatsQuestionSimpleWrapper>> getUserStatsLess(@Path("id") int userId);

    @GET(URLService.STATS_USER_LESS)
    Single<StatsWrapper<StatsQuestionSimpleWrapper>> getUserStatsLessByPage(@Path("id") int userId,
            @Query("page") int page);

    @POST(URLService.STATS_USER)
    Completable postStatsForId(@Path("id") int userId,
            @Body StatsPostSimpleWrapper stats);

    /**
     * Factory class to get a singleton instance of {@link StatsService}
     */
    class Client extends AbstractClient<StatsService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(StatsService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
