/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.api;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import ro.tasegula.carbon.android.rest.utils.Status;

public interface FileUploadService {

    @Multipart
    @POST("/upload")
    Single<Status> upload(@Part("file") MultipartBody.Part image);

    /**
     * Factory class to get a singleton instance of {@link FileUploadService}
     */
    class Client extends AbstractClient<FileUploadService> {
        private static Client instance = null;

        private Client() {
            api = getRetrofit().create(FileUploadService.class);
        }

        public static synchronized Client getInstance() {
            if (instance == null) {
                instance = new Client();
            }
            return instance;
        }
    }
}
