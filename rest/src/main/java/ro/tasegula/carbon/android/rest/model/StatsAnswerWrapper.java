/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.android.rest.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class StatsAnswerWrapper {

	private long statsQuestionId;

	private Timestamp answeredAt;
	private boolean isCorrect;

	List<String> answers = new ArrayList<>();

	public long getStatsQuestionId() {
		return statsQuestionId;
	}

	public void setStatsQuestionId(long id) {
		this.statsQuestionId = id;
	}

	public Timestamp getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(Timestamp answeredAt) {
		this.answeredAt = answeredAt;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setIsCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}
}
