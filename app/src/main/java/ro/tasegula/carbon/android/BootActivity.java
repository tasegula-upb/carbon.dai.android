package ro.tasegula.carbon.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;

import ro.tasegula.carbon.android.app.main.MainActivity;
import ro.tasegula.carbon.android.dagger.Components;

public class BootActivity extends AppCompatActivity {
    /**
     * Starts the BootActivity.
     *
     * @param parent the {@link Context} from which to start this {@link android.app.Activity}.
     */
    public static void start(Context parent) {
        Bundle bundle = ActivityOptionsCompat
                .makeCustomAnimation(parent,
                        R.anim.main_in,
                        R.anim.main_out)
                .toBundle();
        parent.startActivity(new Intent(parent, BootActivity.class), bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDagger();
        initExtra();
        MainActivity.start(this);
        finish();
    }

    private void initDagger() {
        if (!App.get().isInitialized()) App.get().initialize();
        Components.get().applicationComponent().buildActivityComponent().inject(this);
    }

    private void initExtra() {
    }
}
