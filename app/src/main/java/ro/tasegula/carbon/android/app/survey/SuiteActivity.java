package ro.tasegula.carbon.android.app.survey;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseActivity;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.util.IntentHelper;

public class SuiteActivity extends BaseActivity {
    private static final Logger sLog = LoggerFactory.getLogger(SuiteActivity.class);

    public static void start(Context parent, SuiteWrapper suite) {
        Intent intent = new Intent(parent, SuiteActivity.class);

        intent.putExtra(IntentHelper.SUITE_ID, (int) suite.getId());
        intent.putExtra(IntentHelper.TITLE, suite.getName());

        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
        setupToolbar();

        // inject
        Components.get().buildAppActivityComponent().inject(this);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        int id = intent.getIntExtra(IntentHelper.SUITE_ID, 0);
        String title = intent.getStringExtra(IntentHelper.TITLE);

        // Set toolbar title
        setTitle(title);

        replaceFragment(SuiteFragment.getInstance(id, title));
    }
}
