package ro.tasegula.carbon.android.util.items;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.util.Utils;

public enum QuestionType {

	SIMPLE(R.drawable.ic_question_type_simple),
	IMAGE(R.drawable.ic_question_type_image),
	COMPLEX(R.drawable.ic_question_type_complex);

	public final int iconId;

	QuestionType(int iconId) {
		this.iconId = iconId;
	}

	public static QuestionType getType(QuestionWrapper question) {
		if (Utils.isStringEmpty(question.getImageUri())) {
			return SIMPLE;
		}
		if (question.getAnswers().size() == 1) {
			return IMAGE;
		}
		return COMPLEX;
	}

	public static int getIcon(String uri) {
		if (Utils.isStringEmpty(uri)) {
			return IMAGE.iconId;
		}
		return SIMPLE.iconId;
	}
}
