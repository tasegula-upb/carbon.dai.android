package ro.tasegula.carbon.android.app.survey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.config.Global;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.SuiteService;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;

public class SuitePresenter extends BasePresenter<SuitePresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(SuitePresenter.class);

    @Inject
    SuiteService mSuiteService;

    private int mSuiteId = 0;
    private int mPreviousTotal = 0;
    private boolean mIsLoading = true;
    private int mPageNumber = Global.DB.FIRST_PAGE;

    @Inject
    SuitePresenter() {
    }

    public void setSuiteId(int suiteId) {
        this.mSuiteId = suiteId;
    }

    public void getNextPageQuestions() {
        mSuiteService.getPageQuestionsForId(mSuiteId, mPageNumber++)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<List<QuestionWrapper>>(sLog) {
                               @Override
                               public void accept(@NonNull List<QuestionWrapper> value) {
                                   getMvpView().updateQuestionsList(value);
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    public void getNextPageIfNeeded(int visibleItemCount, int totalItemCount, int firstVisibleItem) {
        if (mIsLoading) {
            if (totalItemCount > mPreviousTotal) {
                mIsLoading = false;
                mPreviousTotal = totalItemCount;
            }
        }
        if (!mIsLoading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + Global.DB.VISIBLE_THRESHOLD)) {
            // End has been reached - Call the server
            getNextPageQuestions();
            mIsLoading = true;
        }
    }

    public interface View extends IMvpView {
        void updateQuestionsList(List<QuestionWrapper> list);
    }
}
