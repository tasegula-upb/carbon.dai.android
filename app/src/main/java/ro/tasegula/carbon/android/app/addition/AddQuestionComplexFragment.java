package ro.tasegula.carbon.android.app.addition;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.common.inputs.InputImageView;
import ro.tasegula.carbon.android.common.inputs.InputSpinnerImageView;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.AnswerWrapper;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class AddQuestionComplexFragment extends BaseAddFragment {
    private static final Logger sLog = LoggerFactory.getLogger(AddQuestionComplexFragment.class);

    @BindView(R.id.questionName)
    InputImageView mQuestionInput;

    @BindView(R.id.questionSuite)
    InputSpinnerImageView mSuiteInput;

    @BindView(R.id.questionHint)
    InputImageView mHintInput;

    private final List<EditText> mAnswerInputs = Arrays.asList(new EditText[5]);
    private final List<Point> mAnswerPosition = Arrays.asList(new Point[5]);
    private int count = 0;

    private ArrayAdapter<String> mAdapter;
    private final List<String> mList = new ArrayList<>();
    private final Map<String, Integer> mSuites = new HashMap<>();

    public static AddQuestionComplexFragment getInstance() {
        Bundle bundle = new Bundle();

        AddQuestionComplexFragment instance = new AddQuestionComplexFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    protected void init(View rootView) {
        Components.get().buildAppActivityComponent().inject(this);
        mPresenter.attachView(this);

        decorateUploadFab();
        decorateSubmit();

        mPresenter.fetchEditableSuites();

        mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSuiteInput.setAdapter(mAdapter);

        mAnswerInputs.set(0, (EditText) rootView.findViewById(R.id.questionAnswerInput1));
        mAnswerInputs.set(1, (EditText) rootView.findViewById(R.id.questionAnswerInput2));
        mAnswerInputs.set(2, (EditText) rootView.findViewById(R.id.questionAnswerInput3));
        mAnswerInputs.set(3, (EditText) rootView.findViewById(R.id.questionAnswerInput4));
        mAnswerInputs.set(4, (EditText) rootView.findViewById(R.id.questionAnswerInput5));

        mImage.setOnTouchListener(new ImageTouchListener());
    }

    @Override
    int getLayoutRes() {
        return R.layout.content_add_complex;
    }

    @Override
    void submit() {
        QuestionWrapper question = buildRequest();
        mPresenter.submitQuestion(question);
    }

    @Override
    public void updateSuitesSpinner(List<SuiteWrapper> suites) {
        mSuites.clear();
        for (SuiteWrapper suite : suites) {
            mSuites.put(suite.getName(), (int) suite.getId());
        }

        mList.clear();
        mList.addAll(mSuites.keySet());
        mAdapter.notifyDataSetChanged();
    }

    @Nullable
    private QuestionWrapper buildRequest() {
        QuestionWrapper question = new QuestionWrapper();
        List<AnswerWrapper> answers = new ArrayList<>();
        boolean isEmpty = false;

        // test image
        if (!mPresenter.hasImage()) {
            toast("A complex image needs an image");
            return null;
        }

        // set question
        String value = mQuestionInput.getValue();
        question.setQuestion(value);
        isEmpty |= value.isEmpty();

        // set answers
        for (int i = 0; i < mAnswerPosition.size(); i++) {
            value = mAnswerInputs.get(i).getText().toString();
            Point pos = mAnswerPosition.get(i);
            if (pos != null)
                answers.add(new AnswerWrapper(value, pos.x, pos.y));
            isEmpty &= value.isEmpty();
        }
        question.setAnswers(answers);

        // set suite
        value = mSuiteInput.getValue();
        if (value.isEmpty())
            isEmpty = true;
        else {
            Integer id = mSuites.get(value);
            if (id != null)
                question.setSuiteId(mSuites.get(value));
            else
                isEmpty = true;
        }

        // check if empty
        if (isEmpty) return null;

        // set hint
        question.setHint(mHintInput.getValue());

        return question;
    }

    // ------------------------------------------------------------------------
    // region IMAGE TOUCH HELPERS
    private boolean isEmpty(EditText edit) {
        return edit.getText().length() > 0;
    }

    private void addEdit(EditText edit, Point point) {
        float x = point.x;
        float y = point.y;

        edit.setHint("Add answer");
        edit.setVisibility(View.VISIBLE);
        edit.setOnFocusChangeListener(new EditTextFocusListener(edit));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                                                             ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = (int) x;
        params.topMargin = (int) y;
        edit.setLayoutParams(params);
        edit.requestFocus();
    }
    // endregion
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // region IMAGE TOUCH LISTENER
    private class ImageTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Point point = new Point((int) event.getX(), (int) event.getY());

            if (count < 5 && !mAnswerPosition.contains(point)) {
                sLog.debug("TOUCH({} / {}) = {}", point.x, point.y, count);

                if (count > 0) mAnswerInputs.get(count - 1).clearFocus();

                addEdit(mAnswerInputs.get(count), point);
                mAnswerPosition.set(count, point);
                count++;
            } else if (!mAnswerPosition.contains(point)) {
                count = 4;
                mAnswerInputs.get(count - 1).clearFocus();

                EditText edit = mAnswerInputs.get(count);
                if (isEmpty(edit)) {
                    mAnswerPosition.set(count, point);
                    addEdit(edit, point);
                }
            }

            return true;
        }
    }

    private class EditTextFocusListener implements View.OnFocusChangeListener {

        EditText text;

        public EditTextFocusListener(EditText text) {
            this.text = text;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (text.getText().length() == 0) {
                    text.setVisibility(View.GONE);
                    sLog.debug("EMPTY-B: C({}) F(-)", count);
                    count = (count > 0) ? count - 1 : 0;
                    sLog.debug("EMPTY-A: C({}) F(-)", count);
                }
                //focus = 0;
                sLog.debug("NO FOCUS: {} / C({}) F(-)", text.getText().toString(), count);
            } else {
                //focus = 1;
                sLog.debug("FOCUS: {} / C({}) F(-)", text.getText().toString(), count);
            }
        }
    }
    // endregion
    // ------------------------------------------------------------------------
}
