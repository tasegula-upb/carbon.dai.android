package ro.tasegula.carbon.android.app.main.discover;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.common.cards.CardCategoryView;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.rest.api.DiscoverService;
import ro.tasegula.carbon.android.rest.model.SetWrapper;

public class DiscoverPresenter extends BasePresenter<DiscoverPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(DiscoverPresenter.class);

    @Inject
    DiscoverService mDiscoverService;

    @Inject
    DiscoverPresenter() {
    }

    void fetchSets() {
        mDiscoverService.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<SetWrapper>>() {
                               @Override
                               public void accept(@NonNull List<SetWrapper> list) throws Exception {
                                   getMvpView().updateSetsList(list);
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    public CardCategoryView.OnClickListener GetCardListener() {
        return new OnClickCard(this);
    }

    // ------------------------------------------------------------------------
    // region Card#OnClickListener
    private static final class OnClickCard implements CardCategoryView.OnClickListener<SetWrapper> {

        private final DiscoverPresenter mPresenter;

        private OnClickCard(DiscoverPresenter presenter) {
            mPresenter = presenter;
        }

        @Override
        public void onView(SetWrapper set) {
            mPresenter.getMvpView().startForSet(set);
        }
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface View extends IMvpView {
        void updateSetsList(List<SetWrapper> list);

        void startForSet(SetWrapper set);
    }
}
