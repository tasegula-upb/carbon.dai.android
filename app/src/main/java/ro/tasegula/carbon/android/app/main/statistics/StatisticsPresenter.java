package ro.tasegula.carbon.android.app.main.statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.common.cards.CardCategoryView;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.rest.api.UserService;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class StatisticsPresenter extends BasePresenter<StatisticsPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(StatisticsPresenter.class);

    @Inject
    UserService mUserService;

    @Inject
    StatisticsPresenter() {
    }

    void fetchSuites() {
        mUserService.getSuitesForId(UserData.ID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<SuiteWrapper>>() {
                               @Override
                               public void accept(@NonNull List<SuiteWrapper> list) throws Exception {
                                   getMvpView().updateSuitesList(list);
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    OnClickCard getCardListener() {
        return new OnClickCard(this);
    }

    // ------------------------------------------------------------------------
    // region Card#OnClickListener
    private static final class OnClickCard implements CardCategoryView.OnClickListener<SuiteWrapper> {

        private final StatisticsPresenter mPresenter;

        private OnClickCard(StatisticsPresenter presenter) {
            mPresenter = presenter;
        }

        @Override
        public void onView(SuiteWrapper suite) {
            mPresenter.getMvpView().showStatsForSuite(suite);
        }
    }
    // endregion
    // ------------------------------------------------------------------------

    interface View extends IMvpView {
        void updateSuitesList(List<SuiteWrapper> list);

        void showStatsForSuite(SuiteWrapper suite);
    }
}
