package ro.tasegula.carbon.android.adapters;

import android.content.Context;

import java.util.List;

import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.common.cards.CardCategoryView;

public class CategorySetsAdapter extends CategoryAdapter<SetWrapper> {

    public CategorySetsAdapter(Context context, List<SetWrapper> list,
            CardCategoryView.OnClickListener<SetWrapper> listener) {
        super(context, list, listener);
    }

    @Override
    protected String getName(SetWrapper object) {
        return object.getName();
    }
}
