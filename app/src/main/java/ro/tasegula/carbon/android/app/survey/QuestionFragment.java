package ro.tasegula.carbon.android.app.survey;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.common.controls.ControlsView;
import ro.tasegula.carbon.android.common.questions.BaseQuestionView;
import ro.tasegula.carbon.android.common.questions.ComplexQuestionView;
import ro.tasegula.carbon.android.common.questions.ImageQuestionView;
import ro.tasegula.carbon.android.common.questions.SimpleQuestionView;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.util.IntentHelper;
import ro.tasegula.carbon.android.config.Settings;
import ro.tasegula.carbon.android.util.items.QuestionType;

public class QuestionFragment extends BaseFragment implements QuestionPresenter.View {

    @Inject
    QuestionPresenter mPresenter;

    @BindView(R.id.questionView_simple)
    SimpleQuestionView mSimpleQuestion;

    @BindView(R.id.questionView_image)
    ImageQuestionView mImageQuestion;

    @BindView(R.id.questionView_complex)
    ComplexQuestionView mComplexQuestion;

    @BindView(R.id.controls)
    ControlsView mControls;

    private BaseQuestionView mCurrentView;

    public static QuestionFragment getInstance(QuestionType type, int questionId) {
        Bundle bundle = new Bundle();
        bundle.putInt(IntentHelper.QUESTION_ID, questionId);
        bundle.putInt(IntentHelper.QUESTION_TYPE, type.ordinal());

        QuestionFragment instance = new QuestionFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_question, container, false);
        ButterKnife.bind(this, rootView);
        init(rootView);

        mControls.setListener(new QuestionControlsListener(mPresenter, this));
        mControls.setRatingType(Settings.getRatingType());
        mControls.setShowCorrectness(Settings.getCorrectnessShown());

        return rootView;
    }

    @Override
    protected void init(View rootView) {
        Components.get().buildAppActivityComponent().inject(this);
        mPresenter.attachView(this);

        int questionId = getArguments().getInt(IntentHelper.QUESTION_ID);
        int type = getArguments().getInt(IntentHelper.QUESTION_TYPE, QuestionType.SIMPLE.ordinal());
        QuestionType questionType = QuestionType.values()[type];

        mCurrentView = mSimpleQuestion;

        startNextQuestion(questionId, questionType);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void startNextQuestion(int questionId, QuestionType questionType) {
//        mCurrentView.clear();
        mCurrentView.hide();
        mCurrentView = null;
        switch (questionType) {
            case IMAGE:
                mCurrentView = mImageQuestion;
                mPresenter.setQuestion(questionId, mImageQuestion);
                break;
            case SIMPLE:
                mCurrentView = mSimpleQuestion;
                mPresenter.setQuestion(questionId, mSimpleQuestion);
                break;
            case COMPLEX:
                mCurrentView = mComplexQuestion;
                mPresenter.setQuestion(questionId, mComplexQuestion);
                break;
        }
        mCurrentView.show();
    }

    @Override
    public void showHint() {
        mCurrentView.showHint(getActivity());
    }

    @Override
    public void showAnswer() {
        mCurrentView.showAnswer();
    }
}
