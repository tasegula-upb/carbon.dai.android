package ro.tasegula.carbon.android;

import android.app.Application;

import ro.tasegula.carbon.android.dagger.ApplicationComponent;
import ro.tasegula.carbon.android.dagger.ComponentFactory;
import ro.tasegula.carbon.android.dagger.Components;

public class App extends Application {

    private static App sInstance;

    private boolean mIsInitialized;

    /**
     * Gets the instance of the active Application.
     */
    public static App get() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public void initialize() {
        if (!mIsInitialized) {
            Components.initialize(this, new ComponentFactory());
            mIsInitialized = true;
        }
    }

    /**
     * @return True, iff the app-level resources (including DI) have been initialized.
     */
    public boolean isInitialized() {
        return mIsInitialized;
    }
}
