package ro.tasegula.carbon.android.app.addition;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseActivity;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.util.IntentHelper;

public class AddActivity extends BaseActivity {
    private static final Logger sLog = LoggerFactory.getLogger(AddActivity.class);

    public static void start(Context parent, ContentType contentType) {
        Intent intent = new Intent(parent, AddActivity.class);

        intent.putExtra(IntentHelper.CONTENT_TYPE, contentType.ordinal());

        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
        setupToolbar();

        // inject
        Components.get().buildAppActivityComponent().inject(this);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        int contentId = intent.getIntExtra(IntentHelper.CONTENT_TYPE, 1);
        if (contentId < 0 || contentId >= 3)
            contentId = 1;

        ContentType contentType = ContentType.values()[contentId];

        // Set toolbar title
        setTitle(contentType.titleRes);

        switch (contentType) {
            case SUITE:
                replaceFragment(AddSuiteFragment.getInstance());
                break;
            case TAGS:
                replaceFragment(AddQuestionComplexFragment.getInstance());
                break;
            case QUESTION:
            default:
                replaceFragment(AddQuestionFragment.getInstance());
        }
    }
}
