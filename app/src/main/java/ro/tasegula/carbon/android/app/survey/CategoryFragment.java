package ro.tasegula.carbon.android.app.survey;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.adapters.SuitesAdapter;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.util.IntentHelper;

public class CategoryFragment extends BaseFragment implements CategoryPresenter.View {
    private static final Logger sLog = LoggerFactory.getLogger(CategoryFragment.class);

    @Inject
    CategoryPresenter mPresenter;

    private SuitesAdapter mAdapter;
    private final List<SuiteWrapper> mList = new ArrayList<>();

    public static CategoryFragment getInstance(int categoryId, String title) {
        Bundle bundle = new Bundle();
        bundle.putInt(IntentHelper.CATEGORY_ID, categoryId);
        bundle.putString(IntentHelper.TITLE, title);

        CategoryFragment instance = new CategoryFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_survey, container, false);
        Components.get().buildAppActivityComponent().inject(this);
        init(rootView);

        return rootView;
    }

    @Override
    protected void init(View root) {
        mPresenter.attachView(this);

        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.recycler_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = SuitesAdapter.getForCategories(getActivity(), mList, mPresenter.GetCardListener());
        recyclerView.setAdapter(mAdapter);

        int categoryId = getArguments().getInt(IntentHelper.CATEGORY_ID);

        mPresenter.fetchSuitesForCategory(categoryId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void updateSuitesList(List<SuiteWrapper> list) {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void startSuiteActivity(SuiteWrapper suite) {
        SuiteActivity.start(getActivity(), suite);
    }

    @Override
    public void startQuestionActivity(QuestionWrapper question) {
        QuestionActivity.start(getActivity(), question);
    }
}
