package ro.tasegula.carbon.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.util.items.QuestionType;
import ro.tasegula.carbon.android.view.holders.QuestionCollapsedHolder;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionCollapsedHolder> {
    private static final Logger sLog = LoggerFactory.getLogger(QuestionsAdapter.class);

    private Context mContext;
    private List<QuestionWrapper> mQuestions;
    private QuestionCollapsedHolder.OnClick mListener;

    public QuestionsAdapter(Context context, List<QuestionWrapper> questions, QuestionCollapsedHolder.OnClick listener) {
        mContext = context;
        mQuestions = questions;
        mListener = listener;
    }

    @Override
    public QuestionCollapsedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_question_collapsed, parent, false);
        return new QuestionCollapsedHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(QuestionCollapsedHolder holder, int position) {
        QuestionWrapper question = mQuestions.get(position);
        holder.setTitle(question.getQuestion());

        QuestionType type = QuestionType.getType(question);
        if (type == QuestionType.IMAGE && question.getImageUri() != null)
            holder.setIcon(question.getImageUri());
        else
            holder.setIcon(type.iconId);

        holder.setListener(mListener, question);
    }

    @Override
    public int getItemCount() {
        return mQuestions.size();
    }
}
