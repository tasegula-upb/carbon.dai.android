package ro.tasegula.carbon.android.app;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Base interface that any class that wants to act as a View in the MVP
 * pattern must implement.
 * Generally this interface will be extended by a more specific interface
 * that then usually will be implemented by an Activity or Fragment.
 */
public interface IMvpView {
    Context context();

    void toast(String text);

    void toast(@StringRes int text);
}
