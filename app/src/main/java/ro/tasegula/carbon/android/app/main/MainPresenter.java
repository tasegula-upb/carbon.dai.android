package ro.tasegula.carbon.android.app.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.StatsService;
import ro.tasegula.carbon.android.rest.model.DatabaseStatsWrapper;

public class MainPresenter extends BasePresenter<MainPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(MainPresenter.class);

    @Inject
    StatsService mStatsService;

    @Inject
    MainPresenter() {
    }

    void getStatsForUser(int id) {
        mStatsService.getUserDbStats(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<DatabaseStatsWrapper>(sLog) {
                    @Override
                    public void accept(@NonNull DatabaseStatsWrapper value) {
                        super.accept(value);
                        getMvpView().updateMainCards(value);
                    }
                }, Consumers.failure(sLog, getMvpView()));
    }

    public interface View extends IMvpView {
        void updateMainCards(DatabaseStatsWrapper stats);
    }
}
