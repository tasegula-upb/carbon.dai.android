package ro.tasegula.carbon.android.app.main.collection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.adapters.SuitesAdapter;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.app.survey.QuestionActivity;
import ro.tasegula.carbon.android.app.survey.SuiteActivity;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class CollectionFragment extends BaseFragment implements CollectionPresenter.View {
    private static final Logger sLog = LoggerFactory.getLogger(CollectionFragment.class);

    @Inject
    CollectionPresenter mPresenter;

    private SuitesAdapter mAdapter;
    private final List<SuiteWrapper> mList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_main_collection, container, false);
        Components.get().buildAppActivityComponent().inject(this);
        ButterKnife.bind(this, rootView);

        setTitle(R.string.drawer_collection);
        init(rootView);

        return rootView;
    }

    @Override
    protected void init(View root) {
        mPresenter.attachView(this);

        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.recycler_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = SuitesAdapter.getForCollection(getActivity(), mList, mPresenter.GetCardListener());
        recyclerView.setAdapter(mAdapter);

        if (UserData.ID > 0)
            mPresenter.fetchSuitesForUser();
        else
            toastMessage(R.string.toast_no_user);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void updateSuitesList(List<SuiteWrapper> list) {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void startSuiteActivity(SuiteWrapper suite) {
        SuiteActivity.start(getActivity(), suite);
    }

    @Override
    public void startQuestionActivity(QuestionWrapper question) {
        QuestionActivity.start(getActivity(), question);
    }
}
