package ro.tasegula.carbon.android.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ro.tasegula.carbon.android.rest.api.AddQuestionService;
import ro.tasegula.carbon.android.rest.api.AddSuiteService;
import ro.tasegula.carbon.android.rest.api.CategoryService;
import ro.tasegula.carbon.android.rest.api.CollectionService;
import ro.tasegula.carbon.android.rest.api.DiscoverService;
import ro.tasegula.carbon.android.rest.api.FileUploadService;
import ro.tasegula.carbon.android.rest.api.QuestionService;
import ro.tasegula.carbon.android.rest.api.StatsService;
import ro.tasegula.carbon.android.rest.api.SuiteService;

@Module
public class RetrofitCallModule {

    public RetrofitCallModule() {
    }

    @Provides
    @Singleton
    AddQuestionService provideAddQuestionServiceApi() {
        return AddQuestionService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    AddSuiteService provideAddSuiteServiceApi() {
        return AddSuiteService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    CategoryService provideCategoryServiceApi() {
        return CategoryService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    CollectionService provideCollectionServiceApi() {
        return CollectionService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    DiscoverService provideDiscoverServiceApi() {
        return DiscoverService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    FileUploadService provideFileUploadServiceApi() {
        return FileUploadService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    QuestionService provideQuestionServiceApi() {
        return QuestionService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    SuiteService provideSuiteServiceApi() {
        return SuiteService.Client.getInstance().getAPI();
    }

    @Provides
    @Singleton
    StatsService provideUserStatsServiceApi() {
        return StatsService.Client.getInstance().getAPI();
    }
}
