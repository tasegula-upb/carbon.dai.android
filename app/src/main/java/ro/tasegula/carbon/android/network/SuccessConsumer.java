package ro.tasegula.carbon.android.network;

import org.slf4j.Logger;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

public class SuccessConsumer<T> implements Consumer<T> {

    private final Logger mLogger;

    public SuccessConsumer(Logger logger) {
        mLogger = logger;
    }

    @Override
    public void accept(@NonNull T value) {
        mLogger.debug("SUCCESS: {}", value);
    }
}
