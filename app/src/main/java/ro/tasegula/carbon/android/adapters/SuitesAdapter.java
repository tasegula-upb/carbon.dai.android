package ro.tasegula.carbon.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.common.cards.CardSuitesView;

public class SuitesAdapter extends RecyclerView.Adapter<SuitesAdapter.ViewHolder> {

    private enum SuiteType {
        CATEGORIES(true), COLLECTION(false);

        final boolean show;

        SuiteType(boolean show) {
            this.show = show;
        }
    }

    private Context mContext;
    private List<SuiteWrapper> mSuites;
    private CardSuitesView.OnClickListener<SuiteWrapper> mListener;

    private SuiteType mSuiteType;

    public SuitesAdapter(Context context, List<SuiteWrapper> suites,
            CardSuitesView.OnClickListener<SuiteWrapper> listener, SuiteType suiteType) {
        mContext = context;
        mSuites = suites;
        mListener = listener;
        mSuiteType = suiteType;
    }

    public static SuitesAdapter getForCategories(Context context, List<SuiteWrapper> suites,
            CardSuitesView.OnClickListener<SuiteWrapper> listener) {
        return new SuitesAdapter(context, suites, listener, SuiteType.CATEGORIES);
    }

    public static SuitesAdapter getForCollection(Context context, List<SuiteWrapper> suites,
            CardSuitesView.OnClickListener<SuiteWrapper> listener) {
        return new SuitesAdapter(context, suites, listener, SuiteType.COLLECTION);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardSuitesView view = new CardSuitesView(mContext);
        view.setMainActionVisibility(mSuiteType.show);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SuiteWrapper suite = mSuites.get(position);
        holder.updateCard(mListener, suite);
    }

    @Override
    public int getItemCount() {
        return mSuites.size();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View itemView) {
            super(itemView);
        }

        void updateCard(CardSuitesView.OnClickListener<SuiteWrapper> listener, SuiteWrapper suite) {
            CardSuitesView view = (CardSuitesView) itemView;
            view.setTitle(suite.getName());
            view.setSubtitle(suite.getDescription());
            view.setImage(suite.getImageUri());

            if (listener != null)
                view.setListener(listener, suite);
        }
    }
}
