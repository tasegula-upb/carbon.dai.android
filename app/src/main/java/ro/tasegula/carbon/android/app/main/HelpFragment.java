package ro.tasegula.carbon.android.app.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.common.about.AboutPage;
import ro.tasegula.carbon.android.common.about.Element;
import ro.tasegula.carbon.android.dagger.Components;

public class HelpFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_main_help, container, false);
        Components.get().buildAppActivityComponent().inject(this);

        setTitle(R.string.drawer_help);
        init(rootView);

        Element versionElement = new Element();
        versionElement.setTitle(getString(R.string.help_thanks));

        return new AboutPage(getActivity())
                .setImage(R.mipmap.ic_launcher)
                .setDescription(getString(R.string.help_description))
                .addItem(versionElement)
                .addGroup(getString(R.string.help_connect))
                .addWebsite(getString(R.string.app_site))
                .addEmail(getString(R.string.app_email))
                .addFacebook(getString(R.string.app_fb))
                .addPlayStore(getString(R.string.app_play))
                .create();
    }

    @Override
    protected void init(View rootView) {
    }
}
