package ro.tasegula.carbon.android.app;

import android.content.Context;

public abstract class BasePresenter<T extends IMvpView> implements IMvpPresenter<T> {

    private T mMvpView;
    protected Context context;

    public void updateContext(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    protected T getMvpView() {
        return mMvpView;
    }

    public void assertViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter#attachView(MvpView) before requesting data to the Presenter");
        }
    }
}
