package ro.tasegula.carbon.android.app.login;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ro.tasegula.carbon.android.R;

public abstract class BaseLoginActivity extends AppCompatActivity implements LoginPresenter.View {

    @Override
    public Context context() {
        return this;
    }

    // region Messages
    @Override
    public void toast(String text) {
        final Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_LONG);
        decorateToast(snackbar);
    }

    @Override
    public void toast(@StringRes int text) {
        final Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_LONG);
        decorateToast(snackbar);
    }

    public void toastMessage(@StringRes int text) {
        final Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_INDEFINITE);
        decorateToast(snackbar);
    }

    private void decorateToast(final Snackbar snackbar) {
        snackbar.setAction(R.string.toast_dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }
    // endregion
}
