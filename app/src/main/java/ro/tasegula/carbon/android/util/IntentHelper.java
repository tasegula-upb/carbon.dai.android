package ro.tasegula.carbon.android.util;

public final class IntentHelper {
    public static final String CATEGORY_ID = "id.category";
    public static final String SUITE_ID = "id.suite";
    public static final String QUESTION_ID = "id.question";
    public static final String TITLE = "title";

    public static final String QUESTION_TYPE = "questionType";
    public static final String CONTENT_TYPE = "contentType";

    private IntentHelper() {
    }
}
