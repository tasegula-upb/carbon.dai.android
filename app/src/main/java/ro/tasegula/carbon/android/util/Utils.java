package ro.tasegula.carbon.android.util;

import org.apache.commons.lang3.StringUtils;

import ro.tasegula.carbon.android.config.Settings;

public class Utils {

    public static boolean isCorrect(String given, String answer) {
        int distance = StringUtils.getLevenshteinDistance(given, answer);
        return distance <= Settings.getCorrectnessDist();
    }

    public static boolean isStringEmpty(String uri) {
        return (uri == null) || (uri.isEmpty());
    }
}
