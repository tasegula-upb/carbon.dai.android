package ro.tasegula.carbon.android.app.survey;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseActivity;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.util.IntentHelper;
import ro.tasegula.carbon.android.util.items.QuestionType;

public class QuestionActivity extends BaseActivity {
    private static final Logger sLog = LoggerFactory.getLogger(QuestionActivity.class);

    @Inject
    QuestionPresenter mPresenter;

    private int mSuiteId, mQuestionId;

    public static void start(Context parent, QuestionWrapper question) {
        Intent intent = new Intent(parent, QuestionActivity.class);

        // get current id and create an empty array
        QuestionType qType = QuestionType.getType(question);

        intent.putExtra(IntentHelper.QUESTION_ID, (int) question.getId());
        intent.putExtra(IntentHelper.SUITE_ID, (int) question.getSuiteId());
        intent.putExtra(IntentHelper.QUESTION_TYPE, qType.ordinal());
        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        // inject
        Components.get().buildAppActivityComponent().inject(this);

        // Get intent, action and MIME type
        QuestionType type = handleIntent(getIntent());

        replaceFragment(QuestionFragment.getInstance(type, mQuestionId));
    }

    QuestionType handleIntent(Intent intent) {
        mSuiteId = intent.getIntExtra(IntentHelper.SUITE_ID, 0);
        mQuestionId = intent.getIntExtra(IntentHelper.QUESTION_ID, 0);

        QuestionType questionType =
                QuestionType.values()[intent.getIntExtra(IntentHelper.QUESTION_TYPE, 0)];

        mPresenter.initialize(mSuiteId, mQuestionId, questionType);

        return questionType;
    }
}
