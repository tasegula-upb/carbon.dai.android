package ro.tasegula.carbon.android.app.main.statistics;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.adapters.CategorySuitesAdapter;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class StatisticsFragment extends BaseFragment implements StatisticsPresenter.View {

    @Inject
    StatisticsPresenter mPresenter;

    private CategorySuitesAdapter mAdapter;
    private final List<SuiteWrapper> mList = new ArrayList<>();

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(R.layout.content_main_statistics, container, false);
        Components.get().buildAppActivityComponent().inject(this);

        setTitle(R.string.drawer_statistics);
        init(rootView);

        return rootView;
    }

    @Override
    protected void init(android.view.View rootView) {
        mPresenter.attachView(this);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new CategorySuitesAdapter(getActivity(), mList, mPresenter.getCardListener());
        recyclerView.setAdapter(mAdapter);

        if (UserData.ID > 0)
            mPresenter.fetchSuites();
        else
            toastMessage(R.string.toast_no_user);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void updateSuitesList(List<SuiteWrapper> list) {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showStatsForSuite(SuiteWrapper suite) {
        pushDescriptionFragment(SuiteStatisticsFragment.getInstance((int) suite.getId()));
    }
}
