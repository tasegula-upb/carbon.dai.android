package ro.tasegula.carbon.android.app.main.discover;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.adapters.CategorySetsAdapter;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.app.survey.CategoryActivity;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.SetWrapper;

public class DiscoverFragment extends BaseFragment implements DiscoverPresenter.View {
    private static final Logger sLog = LoggerFactory.getLogger(DiscoverFragment.class);

    @Inject
    DiscoverPresenter mPresenter;

    private CategorySetsAdapter mAdapter;
    private final List<SetWrapper> mList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_main_discover, container, false);
        Components.get().buildAppActivityComponent().inject(this);
        ButterKnife.bind(this, rootView);

        setTitle(R.string.drawer_discover);
        init(rootView);

        return rootView;
    }

    @Override
    protected void init(View root) {
        mPresenter.attachView(this);

        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.recycler_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new CategorySetsAdapter(getActivity(), mList, mPresenter.GetCardListener());
        recyclerView.setAdapter(mAdapter);

        mPresenter.fetchSets();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void updateSetsList(List<SetWrapper> list) {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void startForSet(SetWrapper set) {
        CategoryActivity.start(getActivity(), set);
    }
}
