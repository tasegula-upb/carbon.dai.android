package ro.tasegula.carbon.android.app.addition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import butterknife.BindView;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.common.inputs.InputImageView;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class AddSuiteFragment extends BaseAddFragment {
    private static final Logger sLog = LoggerFactory.getLogger(AddSuiteFragment.class);

    @BindView(R.id.suiteName)
    InputImageView mNameInput;

    @BindView(R.id.suiteDescription)
    InputImageView mDescriptionInput;

    public static AddSuiteFragment getInstance() {
        Bundle bundle = new Bundle();

        AddSuiteFragment instance = new AddSuiteFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    protected void init(View rootView) {
        Components.get().buildAppActivityComponent().inject(this);
        mPresenter.attachView(this);

        decorateUploadFab();
        decorateSubmit();
    }

    @Override
    int getLayoutRes() {
        return R.layout.content_add_suite;
    }

    @Override
    void submit() {
        mPresenter.submitSuite(buildRequest());
    }

    @Override
    public void updateSuitesSpinner(List<SuiteWrapper> suites) {
        // do nothing
    }

    @Nullable
    private SuiteWrapper buildRequest() {
        SuiteWrapper suite = new SuiteWrapper();
        boolean isEmpty = false;

        // set question
        String value = mNameInput.getValue();
        suite.setName(value);
        isEmpty |= value.isEmpty();

        // set answer
        value = mDescriptionInput.getValue();
        suite.setDescription(value);
        isEmpty |= value.isEmpty();

        // check if empty
        if (isEmpty) return null;

        return suite;
    }
}
