package ro.tasegula.carbon.android.network;

import org.slf4j.Logger;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import ro.tasegula.carbon.android.app.IMvpView;

public class FailureConsumer implements Consumer<Throwable> {

    private final Logger mLogger;
    private final IMvpView mMvpView;
    private final String mToastMessage;

    public FailureConsumer(Logger logger, IMvpView mvpView) {
        mLogger = logger;
        mMvpView = mvpView;
        mToastMessage = "ERROR!";
    }

    public FailureConsumer(Logger logger, IMvpView mvpView, String toastMessage) {
        mLogger = logger;
        mMvpView = mvpView;
        mToastMessage = toastMessage;
    }

    @Override
    public void accept(@NonNull Throwable t) {
        mLogger.error("FAILED: {}", t);

        if (mMvpView != null)
            mMvpView.toast(mToastMessage);
    }
}
