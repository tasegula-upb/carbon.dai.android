package ro.tasegula.carbon.android.app.main;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseActivity;
import ro.tasegula.carbon.android.app.DrawerItems;
import ro.tasegula.carbon.android.config.Global;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.config.Settings;

public class MainActivity
        extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    private static ActionBarDrawerToggle drawerToggle;

    @Inject
    SharedPreferences mPreferences;

    public static void start(Context parent) {
        Intent intent = new Intent(parent, MainActivity.class);
        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // inject
        ButterKnife.bind(this);
        Components.get().buildAppActivityComponent().inject(this);

        // setup toolbar
        Toolbar toolbar = setupToolbar();
        drawerToggle = setupNavigationDrawer(drawer, toolbar);

        // setup navigation
        ProfileBoxHolder profileHolder = setupNavigationView(this,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (drawerToggle.isDrawerIndicatorEnabled()) {
                                        changeFragment(R.id.nav_main);
                                        drawer.closeDrawers();
                                    }
                                }
                            });

        // setup content
        changeFragment(R.id.nav_main);

        // find if user is logged in
        UserData.getFromSharedPreferences(mPreferences);
        if (!UserData.isLogged()) {
//            startActivity(new Intent(this, LoginActivity.class));
        }

        profileHolder.setImage(this, UserData.googleImage);
        profileHolder.setName(UserData.googleName);
        profileHolder.setEmail(UserData.googleEmail);

        // UPDATE preferences
        Settings.updatePreferences(this);
    }

    // ----------------------------------------------------
    // region NavigationView.OnNavigationItemSelectedListener
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        closeDrawer();

        switch (id) {
            case R.id.nav_feedback:
                return startFeedbackIntent();
            case R.id.nav_share:
                return startShareIntent();
            default:
                return changeFragment(id);
        }
    }

    private void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private boolean changeFragment(@IdRes int id) {
        Fragment fragment = DrawerItems.getFragment(id);
        replaceFragment(fragment);
        return true;
    }

    private boolean startFeedbackIntent() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
                                        Uri.fromParts("mailto", Global.ContactData.EMAIL, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, Global.ContactData.EMAIL_SUBJECT);
        emailIntent.putExtra(Intent.EXTRA_TEXT, Global.ContactData.EMAIL_BODY);
        startActivity(Intent.createChooser(emailIntent, Global.ContactData.EMAIL_EXPLAIN));

        return true;
    }

    private boolean startShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Global.ContactData.SHARE_TEXT);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
        return true;
    }
    // endregion
    // ----------------------------------------------------

    // ----------------------------------------------------
    // region Navigation Setup

    protected ActionBarDrawerToggle setupNavigationDrawer(DrawerLayout drawer, Toolbar toolbar) {
        ActionBarDrawerToggle drawerToggle =
                new ActionBarDrawerToggle(this, drawer, toolbar,
                                          R.string.navigation_drawer_open,
                                          R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        return drawerToggle;
    }

    protected ProfileBoxHolder setupNavigationView(
            NavigationView.OnNavigationItemSelectedListener drawerListener,
            View.OnClickListener headerListener) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(drawerListener);
        navigationView.setItemIconTintList(null);

        View profileBoxView = navigationView.getHeaderView(0);
        profileBoxView.setOnClickListener(headerListener);

        return new ProfileBoxHolder(profileBoxView);
    }
    // endregion
    // ----------------------------------------------------
}
