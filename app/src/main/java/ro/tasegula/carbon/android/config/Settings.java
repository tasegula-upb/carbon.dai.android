package ro.tasegula.carbon.android.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.common.controls.RatingType;

public class Settings {
    private static final Logger sLog = LoggerFactory.getLogger(Settings.class);

    static SharedPreferences preferences = null;

    static String username = UserData.googleName;
    static boolean crashReport = true;
    static boolean correctnessShown = true;
    static int correctDist = 0;
    static boolean ratingSet = false;
    static RatingType ratingType = RatingType.NONE;

    // ------------------------------------------------------------------------
    // SET PREFERENCES
    // ------------------------------------------------------------------------

    public static SharedPreferences getPreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences;
    }

    public static void setPreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void updatePreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        checkPreferences();
        username = preferences.getString(Prefs.USERNAME, UserData.googleName);
        if (UserData.googleName != null && username.equals("")) {
            username = UserData.googleName;
            // add in preferences too
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Prefs.USERNAME, username);
            editor.commit();
        }
        crashReport = preferences.getBoolean(Prefs.CRASH_REPORT, true);

        correctnessShown = preferences.getBoolean(Prefs.CORRECTNESS, true);
        correctDist = preferences.getInt(Prefs.CORRECT_DIST, 0);

        ratingSet = preferences.getBoolean(Prefs.RATING_SET, false);
        ratingType = RatingType.fromPreference(preferences.getString(Prefs.RATING_TYPE, null));
    }

    // ------------------------------------------------------------------------
    // GETTERS FROM PREFERENCES
    // ------------------------------------------------------------------------

    public static String getUsernamePref() {
        checkPreferences();
        username = preferences.getString(Prefs.USERNAME, UserData.googleName);
        return username;
    }

    public static boolean getCrashReportPref() {
        checkPreferences();
        crashReport = preferences.getBoolean(Prefs.CRASH_REPORT, true);
        return crashReport;
    }

    public static boolean getCorrectnessShownPref() {
        checkPreferences();
        correctnessShown = preferences.getBoolean(Prefs.CORRECTNESS, false);
        return correctnessShown;
    }

    public static int getCorrectnessDistPref() {
        checkPreferences();
        correctDist = preferences.getInt(Prefs.CORRECT_DIST, 0);
        return correctDist;
    }

    public static boolean getRatingSettingPref() {
        checkPreferences();
        ratingSet = preferences.getBoolean(Prefs.RATING_SET, false);
        return ratingSet;
    }

    public static RatingType getRatingTypePref() {
        checkPreferences();
        ratingType = RatingType.fromPreference(preferences.getString(Prefs.RATING_TYPE, null));
        return ratingType;
    }

    // ------------------------------------------------------------------------
    // GETTERS
    // ------------------------------------------------------------------------

    public static String getUsername() {
        return username;
    }

    public static boolean getCrashReport() {
        return crashReport;
    }

    public static boolean getCorrectnessShown() {
        return correctnessShown;
    }

    public static int getCorrectnessDist() {
        return correctDist;
    }

    public static boolean getRatingSetting() {
        return ratingSet;
    }

    public static RatingType getRatingType() {
        return ratingType;
    }

    // ------------------------------------------------------------------------
    // PRIVATE HELPERS
    // ------------------------------------------------------------------------

    private static void checkPreferences() {
        if (preferences == null) {
            throw new NullPointerException("Settings preferences is null. Set first and then use it.");
        }
    }

    public static void reset() {
        username = UserData.googleName;
        crashReport = true;
        correctnessShown = true;
        correctDist = 0;
        ratingSet = false;
        ratingType = RatingType.NONE;
    }

    public static final class Prefs {

        public static final String USERNAME = "prefUsername";
        public static final String CRASH_REPORT = "prefCrash";
        public static final String CORRECTNESS = "prefCorrectness";
        public static final String CORRECT_DIST = "prefCorrectDist";
        public static final String RATING_SET = "prefRating";
        public static final String RATING_TYPE = "prefRatingType";
    }
}
