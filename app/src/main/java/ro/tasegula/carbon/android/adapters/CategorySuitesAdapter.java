package ro.tasegula.carbon.android.adapters;

import android.content.Context;

import java.util.List;

import ro.tasegula.carbon.android.common.cards.CardCategoryView;
import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class CategorySuitesAdapter extends CategoryAdapter<SuiteWrapper> {

    public CategorySuitesAdapter(Context context, List<SuiteWrapper> list,
            CardCategoryView.OnClickListener<SuiteWrapper> listener) {
        super(context, list, listener);
    }

    @Override
    protected String getName(SuiteWrapper object) {
        return object.getName();
    }
}
