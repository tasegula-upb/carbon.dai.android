package ro.tasegula.carbon.android.app;

public interface IMvpPresenter<V extends IMvpView> {

    void attachView(V mvpView);

    void detachView();

    boolean isViewAttached();
}
