package ro.tasegula.carbon.android.app.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.dagger.Components;

public class LoginActivity extends BaseLoginActivity
        implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {
    private static final Logger sLog = LoggerFactory.getLogger(LoginActivity.class);
    private static final int RC_SIGN_IN = 9001;

    @BindView(R.id.login_signIn)
    SignInButton mSignInButton;
    @BindView(R.id.login_signOut)
    Button mSignOutButton;
    @BindView(R.id.login_text)
    TextView mStatus;
    @BindView(R.id.login_title)
    TextView mTitle;
    @BindView(R.id.login_subtitle)
    TextView mSubtitle;

    @Inject
    LoginPresenter mPresenter;

    private ProgressDialog mProgressDialog;

    // the service connection to Google Play Services
    private GoogleApiClient mGoogleApiClient;

    public static void start(Context parent) {
        Intent intent = new Intent(parent, LoginActivity.class);
        parent.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // inject
        ButterKnife.bind(this);
        Components.get().buildAppActivityComponent().inject(this);

        mPresenter.attachView(this);

        setCard();
        mGoogleApiClient = setupGoogle();

        mSignInButton.setSize(SignInButton.SIZE_STANDARD);
        mSignInButton.setOnClickListener(this);
        mSignOutButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            sLog.info("Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        sLog.info("handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            mPresenter.signIn(acct);
            update(true);
        } else {
            // Signed out, show unauthenticated UI.
            update(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_signIn:
                mStatus.setText("Signing in ... ");
                signIn();
                break;
            case R.id.login_signOut:
                signOut();
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        update(false);
                    }
                });
        mPresenter.signOut();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        sLog.error("onConnectionFailed:" + connectionResult);
    }

    // ------------------------------------------------------------------------
    // region Progress Dialog
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
    // endregion
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // region Private Helpers
    private GoogleApiClient setupGoogle() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        return new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    @Override
    public void update(boolean signedIn) {
        if (signedIn) {
            mSignInButton.setVisibility(View.GONE);
            mSignOutButton.setVisibility(View.VISIBLE);
        } else {
            mStatus.setText(R.string.login_signOut);

            mSignInButton.setVisibility(View.VISIBLE);
            mSignOutButton.setVisibility(View.GONE);
        }

        setCard();
    }

    private void setCard() {
        String username = "User";
        String data = "";
        int res = R.string.login_text2_out;

        if (UserData.isLogged()) {
            username = UserData.googleName;
            res = R.string.login_text2_in;
            data = String.format("signed in as %s @ %s \n %s",
                                 UserData.googleName,
                                 UserData.googleId,
                                 UserData.googleEmail);
        }

        mTitle.setText(String.format(getResources().getString(R.string.login_text1_greeting), username));
        mSubtitle.setText(getResources().getString(res));
        mStatus.setText(data);
    }
    // endregion
    // ------------------------------------------------------------------------
}
