package ro.tasegula.carbon.android.network;

import org.slf4j.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractCallback<T> implements Callback<T> {

    protected static final String ERROR_FORMAT = "ERROR-{]: {}";
    protected static final String RESPONSE_FORMAT = "RESPONSE-{}: {}";
    protected static final String EXCEPTION_FORMAT = "EXCEPTION: {}\n{}";

    protected abstract Logger getLogger();

    protected void onError(Response<T> response) {
    }

    protected void onSuccess(T response) {
    }

    protected void onException(Throwable t) {
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (!response.isSuccessful()) {
            getLogger().error(ERROR_FORMAT, response.code(), call.request().url());
            onError(response);
        } else {
            getLogger().info(RESPONSE_FORMAT, response.code(), call.request().url());
            onSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        String url = call.request().url().toString();
        getLogger().error(EXCEPTION_FORMAT, url, t.getMessage());
        onException(t);
    }
}
