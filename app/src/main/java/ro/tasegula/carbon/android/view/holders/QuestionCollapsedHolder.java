package ro.tasegula.carbon.android.view.holders;

import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.common.utils.ImageHelper;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;

public final class QuestionCollapsedHolder
        extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    @BindView(R.id.question_collapsed_icon)
    ImageView mImage;

    @BindView(R.id.question_collapsed_title)
    TextView mTitle;

    private OnClick mListener;
    private QuestionWrapper mQuestion;

    private ImageHelper mImageHelper;

    public QuestionCollapsedHolder(View rootView) {
        super(rootView);
        ButterKnife.bind(this, rootView);

        mImageHelper = ImageHelper.get(rootView.getContext());
        itemView.setOnClickListener(this);
    }

    public void setTitle(String name) {
        mTitle.setText(name);
    }

    public void setIcon(@DrawableRes int icon) {
        mImage.setImageResource(icon);
    }

    public void setIcon(String partialUri) {
        mImageHelper.load(partialUri).into(mImage);
    }

    public void setListener(OnClick listener, QuestionWrapper question) {
        mQuestion = question;
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onView(mQuestion);
        }
    }

    public interface OnClick {
        void onView(QuestionWrapper question);
    }
}
