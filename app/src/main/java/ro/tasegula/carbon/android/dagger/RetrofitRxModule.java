package ro.tasegula.carbon.android.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ro.tasegula.carbon.android.rest.api.UserService;

@Module
public class RetrofitRxModule {

    public RetrofitRxModule() {
    }

    @Provides
    @Singleton
    UserService provideUserServiceApi() {
        return UserService.Client.getInstance().getAPI();
    }
}
