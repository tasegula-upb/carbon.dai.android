package ro.tasegula.carbon.android.app;

import android.app.Fragment;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.main.collection.CollectionFragment;
import ro.tasegula.carbon.android.app.main.discover.DiscoverFragment;
import ro.tasegula.carbon.android.app.main.HelpFragment;
import ro.tasegula.carbon.android.app.main.MainFragment;
import ro.tasegula.carbon.android.app.main.SettingsFragment;
import ro.tasegula.carbon.android.app.main.statistics.StatisticsFragment;

public class DrawerItems {

    private static final SparseArray<DrawerFragment> fragments = new SparseArray<>();

    static {
        fragments.put(R.id.nav_main, DrawerItems.Fragments.MAIN);
        fragments.put(R.id.nav_collection, DrawerItems.Fragments.COLLECTION);
        fragments.put(R.id.nav_discover, DrawerItems.Fragments.DISCOVER);
        fragments.put(R.id.nav_statistics, DrawerItems.Fragments.STATISTICS);
        fragments.put(R.id.nav_help, DrawerItems.Fragments.HELP);
        fragments.put(R.id.nav_settings, DrawerItems.Fragments.SETTINGS);
    }

    @NonNull
    public static Fragment getFragment(@IdRes int id) {
        return fragments.get(id).getPage();
    }

    private static abstract class DrawerFragment {
        public final int title;

        DrawerFragment(int title) {
            this.title = title;
        }

        public abstract Fragment getPage();
    }

    private static abstract class DrawerActivity {
        public final int title;

        DrawerActivity(int title) {
            this.title = title;
        }

        public abstract AppCompatActivity getPage();
    }

    // region ITEMS
    private interface Fragments {
        DrawerFragment MAIN = new DrawerFragment(R.string.app_name) {
            public Fragment getPage() {
                return new MainFragment();
            }
        };
        DrawerFragment COLLECTION = new DrawerFragment(R.string.drawer_collection) {
            public Fragment getPage() {
                return new CollectionFragment();
            }
        };
        DrawerFragment DISCOVER = new DrawerFragment(R.string.drawer_discover) {
            public Fragment getPage() {
                return new DiscoverFragment();
            }
        };
        DrawerFragment STATISTICS = new DrawerFragment(R.string.drawer_statistics) {
            public Fragment getPage() {
                return new StatisticsFragment();
            }
        };
        DrawerFragment HELP = new DrawerFragment(R.string.drawer_help) {
            public Fragment getPage() {
                return new HelpFragment();
            }
        };
        DrawerFragment SETTINGS = new DrawerFragment(R.string.drawer_settings) {
            public Fragment getPage() {
                return new SettingsFragment();
            }
        };
    }
    // endregion
}
