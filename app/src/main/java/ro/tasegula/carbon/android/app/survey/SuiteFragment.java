package ro.tasegula.carbon.android.app.survey;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.adapters.QuestionsAdapter;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.util.IntentHelper;
import ro.tasegula.carbon.android.view.holders.QuestionCollapsedHolder;

public class SuiteFragment extends BaseFragment implements SuitePresenter.View {
    private static final Logger sLog = LoggerFactory.getLogger(SuiteFragment.class);

    @Inject
    SuitePresenter mPresenter;

    private QuestionsAdapter mAdapter;
    private final List<QuestionWrapper> mList = new ArrayList<>();

    public static SuiteFragment getInstance(int suiteId, String title) {
        Bundle bundle = new Bundle();
        bundle.putInt(IntentHelper.SUITE_ID, suiteId);
        bundle.putString(IntentHelper.TITLE, title);

        SuiteFragment instance = new SuiteFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_survey, container, false);
        Components.get().buildAppActivityComponent().inject(this);
        init(rootView);

        // Call to server
        mPresenter.setSuiteId(getArguments().getInt(IntentHelper.SUITE_ID, 0));
        mPresenter.getNextPageQuestions();

        return rootView;
    }

    @Override
    protected void init(View rootView) {
        mPresenter.attachView(this);

        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                mPresenter.getNextPageIfNeeded(
                        layoutManager.getChildCount(),
                        layoutManager.getItemCount(),
                        layoutManager.findFirstVisibleItemPosition()
                );
            }
        });

        mAdapter = new QuestionsAdapter(getActivity(), mList,
                                        new QuestionCollapsedHolder.OnClick() {
                                            @Override
                                            public void onView(QuestionWrapper question) {
                                                if (UserData.ID == 0) {
                                                    toast(R.string.toast_no_user);
                                                    return;
                                                }

                                                openQuestion(question);
                                            }
                                        });
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void updateQuestionsList(List<QuestionWrapper> list) {
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    public void openQuestion(QuestionWrapper question) {
        QuestionActivity.start(getActivity(), question);
    }
}
