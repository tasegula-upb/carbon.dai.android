package ro.tasegula.carbon.android.app.addition;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.common.utils.ImageHelper;

import static android.app.Activity.RESULT_OK;

public abstract class BaseAddFragment extends BaseFragment implements AddPresenter.View {
    private static final int SELECT_PICTURE = 1;

    @BindView(R.id.contentAdd_image)
    ImageView mImage;

    @BindView(R.id.contentAdd_upload)
    FloatingActionButton mFab;

    @BindView(R.id.contentAdd_submit)
    Button mSubmit;

    @Inject
    AddPresenter mPresenter;

    ImageHelper mImageHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutRes(), container, false);
        ButterKnife.bind(this, rootView);

        mImageHelper = ImageHelper.get(getActivity());
        init(rootView);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PICTURE:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    mPresenter.setImageFile(selectedImage);

                    mImageHelper.load(selectedImage).into(mImage);
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void exit() {
        getActivity().finish();
    }

    // ------------------------------------------------------------------------
    // region TO IMPLEMENT
    abstract
    @LayoutRes
    int getLayoutRes();

    abstract void submit();
    // endregion
    // ------------------------------------------------------------------------

    void decorateUploadFab() {
        mFab.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(
                        Intent.createChooser(intent, "Select Picture"),
                        SELECT_PICTURE);
            }
        });
    }

    void decorateSubmit() {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }
}
