package ro.tasegula.carbon.android.dagger;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ActivityModule.class,
        RetrofitCallModule.class,
        RetrofitRxModule.class
})
public interface ApplicationComponent {

    ActivityComponent buildActivityComponent();

}