package ro.tasegula.carbon.android.app.main.statistics;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.adapters.StatsAdapter;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.StatsQuestionSimpleWrapper;
import ro.tasegula.carbon.android.util.IntentHelper;

public class SuiteStatisticsFragment extends BaseFragment implements SuiteStatisticsPresenter.View {
    @Inject
    SuiteStatisticsPresenter mPresenter;

    private StatsAdapter mAdapter;
    private final List<StatsQuestionSimpleWrapper> mList = new ArrayList<>();

    public static SuiteStatisticsFragment getInstance(int suiteId) {
        Bundle bundle = new Bundle();
        bundle.putInt(IntentHelper.SUITE_ID, suiteId);

        SuiteStatisticsFragment instance = new SuiteStatisticsFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_main_statistics, container, false);
        Components.get().buildAppActivityComponent().inject(this);

        setTitle(R.string.drawer_statistics);
        init(rootView);

        return rootView;
    }

    @Override
    protected void init(View rootView) {
        mPresenter.attachView(this);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new StatsAdapter(getActivity(), mList);
        recyclerView.setAdapter(mAdapter);

        int suiteId = getArguments().getInt(IntentHelper.SUITE_ID);

        mPresenter.fetchStats(suiteId);
    }

    @Override
    public void updateStats(List<StatsQuestionSimpleWrapper> list) {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }
}
