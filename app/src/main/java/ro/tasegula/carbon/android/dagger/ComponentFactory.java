package ro.tasegula.carbon.android.dagger;

import android.content.Context;

/**
 * Default ComponentFactory for when the app is running normally (as opposed to testing).
 */
public class ComponentFactory implements Components.Factory {

    @Override
    public ApplicationComponent createApplicationComponent(Context context) {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(context))
                .activityModule(new ActivityModule())
                .build();
    }
}
