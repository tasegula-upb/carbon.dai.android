package ro.tasegula.carbon.android.app.main;

import android.animation.Animator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.app.DrawerItems;
import ro.tasegula.carbon.android.app.addition.AddActivity;
import ro.tasegula.carbon.android.app.addition.ContentType;
import ro.tasegula.carbon.android.app.login.LoginActivity;
import ro.tasegula.carbon.android.common.cards.CardBasicActionView;
import ro.tasegula.carbon.android.common.cards.CardBasicView;
import ro.tasegula.carbon.android.config.Settings;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.DatabaseStatsWrapper;

import static android.view.View.GONE;

public class MainFragment extends BaseFragment implements MainPresenter.View {
    private static final Logger sLog = LoggerFactory.getLogger(MainFragment.class);

    @BindView(R.id.card_profile)
    CardBasicView mCardProfile;
    @BindView(R.id.card_collection)
    CardBasicActionView mCardCollection;
    @BindView(R.id.card_discover)
    CardBasicActionView mCardDiscover;

    @BindView(R.id.fab)
    FloatingActionButton mFab;
    @BindView(R.id.fabBackground)
    View mFabBackground;
    @BindView(R.id.fab1_tags)
    LinearLayout mFabTags;
    @BindView(R.id.fab2_question)
    LinearLayout mFabQuestion;
    @BindView(R.id.fab3_suite)
    LinearLayout mFabSuite;

    boolean mIsFabOpen = false;
    int mAnimationY, mAnimationStart;

    @Inject
    MainPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_main, container, false);
        Components.get().buildAppActivityComponent().inject(this);
        ButterKnife.bind(this, rootView);

        setTitle(R.string.app_name);
        init(rootView);

        return rootView;
    }

    @Override
    protected void init(View root) {
        mPresenter.attachView(this);

        // MAIN CARD
        mCardProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.start(getActivity());
            }
        });

        // COLLECTION CARD
        mCardCollection.setAction(new CardBasicActionView.OnClickListener() {
            @Override
            public void onAction() {
                pushDescriptionFragment(DrawerItems.getFragment(R.id.nav_collection));
            }
        });

        // DISCOVER CARD
        mCardDiscover.setAction(new CardBasicActionView.OnClickListener() {
            @Override
            public void onAction() {
                pushDescriptionFragment(DrawerItems.getFragment(R.id.nav_discover));
            }
        });

        mPresenter.getStatsForUser(UserData.ID);

        initFabMenu();
        updateView();
    }

    private void updateView() {
        mCardProfile.setTitle(getString(R.string.main_cardProfile, Settings.getUsername()));

        if (UserData.isLogged()) {
            mCardProfile.setSubtitle(R.string.main_cardProfileText);
            mCardCollection.setVisibility(View.VISIBLE);
            mFab.setVisibility(View.VISIBLE);
        }
        else {
            mCardProfile.setSubtitle(R.string.main_cardProfileText_unregistered);
            mCardCollection.setVisibility(View.GONE);

            mFab.setVisibility(GONE);
            toastMessage(R.string.toast_no_user);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void updateMainCards(DatabaseStatsWrapper stats) {
        mCardCollection.setSubtitle(getString(R.string.main_cardCollectionData,
                                              stats.getNoSuites(),
                                              stats.getNoQuestions()));

        mCardDiscover.setSubtitle(getString(R.string.main_cardDiscoverData,
                                            stats.getNoPublicSuites(),
                                            stats.getNoPulicCategories()));
    }

    // region FAB
    private void initFabMenu() {
        mAnimationY = getActivity().getResources().getDimensionPixelOffset(R.dimen.fab_animation_height);
        mAnimationStart = mAnimationY + getActivity().getResources().getDimensionPixelOffset(R.dimen.fab_padding_fab);

        // initiate fabs
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mIsFabOpen) {
                    showFabMenu();
                } else {
                    closeFabMenu();
                }
            }
        });

        mFabTags.setOnClickListener(new FabMenuItemListener(this, ContentType.TAGS));
        mFabQuestion.setOnClickListener(new FabMenuItemListener(this, ContentType.QUESTION));
        mFabSuite.setOnClickListener(new FabMenuItemListener(this, ContentType.SUITE));

        mFabBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeFabMenu();
            }
        });
    }

    private void showFabMenu() {
        mIsFabOpen = true;
        mFabBackground.setVisibility(View.VISIBLE);
        mFabTags.setVisibility(View.VISIBLE);
        mFabQuestion.setVisibility(View.VISIBLE);
        mFabSuite.setVisibility(View.VISIBLE);

        mFab.animate().rotationBy(180);
        mFabTags.animate().translationY(-mAnimationStart);
        mFabQuestion.animate().translationY(-mAnimationStart - mAnimationY);
        mFabSuite.animate().translationY(-mAnimationStart - mAnimationY - mAnimationY);
    }

    private void closeFabMenu() {
        mIsFabOpen = false;
        mFabBackground.setVisibility(GONE);
        mFab.animate().rotationBy(-180);
        mFabTags.animate().translationY(0);
        mFabQuestion.animate().translationY(0);
        mFabSuite.animate().translationY(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!mIsFabOpen) {
                    mFabTags.setVisibility(GONE);
                    mFabQuestion.setVisibility(GONE);
                    mFabSuite.setVisibility(GONE);
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private static class FabMenuItemListener implements View.OnClickListener {

        private final MainFragment mFragment;
        private final ContentType mContentType;

        private FabMenuItemListener(MainFragment fragment, ContentType contentType) {
            mFragment = fragment;
            mContentType = contentType;
        }

        @Override
        public void onClick(View v) {
            AddActivity.start(mFragment.getActivity(), mContentType);
            mFragment.closeFabMenu();
        }
    }
    // endregion
}
