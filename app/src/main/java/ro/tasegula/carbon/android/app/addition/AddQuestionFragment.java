package ro.tasegula.carbon.android.app.addition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.common.inputs.InputImageView;
import ro.tasegula.carbon.android.common.inputs.InputSpinnerImageView;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.AnswerWrapper;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class AddQuestionFragment extends BaseAddFragment {
    private static final Logger sLog = LoggerFactory.getLogger(AddQuestionFragment.class);

    @BindView(R.id.questionName)
    InputImageView mQuestionInput;

    @BindView(R.id.questionAnswer)
    InputImageView mAnswerInput;

    @BindView(R.id.questionSuite)
    InputSpinnerImageView mSuiteInput;

    @BindView(R.id.questionHint)
    InputImageView mHintInput;

    private ArrayAdapter<String> mAdapter;
    private final List<String> mList = new ArrayList<>();
    private final Map<String, Integer> mSuites = new HashMap<>();

    public static AddQuestionFragment getInstance() {
        Bundle bundle = new Bundle();

        AddQuestionFragment instance = new AddQuestionFragment();
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    protected void init(View rootView) {
        Components.get().buildAppActivityComponent().inject(this);
        mPresenter.attachView(this);

        decorateUploadFab();
        decorateSubmit();

        mPresenter.fetchEditableSuites();

        mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSuiteInput.setAdapter(mAdapter);
    }

    @Override
    int getLayoutRes() {
        return R.layout.content_add_question;
    }

    @Override
    void submit() {
        mPresenter.submitQuestion(buildRequest());
    }

    @Override
    public void updateSuitesSpinner(List<SuiteWrapper> suites) {
        mSuites.clear();
        for (SuiteWrapper suite : suites) {
            mSuites.put(suite.getName(), (int) suite.getId());
        }

        mList.clear();
        mList.addAll(mSuites.keySet());
        mAdapter.notifyDataSetChanged();
    }

    @Nullable
    private QuestionWrapper buildRequest() {
        QuestionWrapper question = new QuestionWrapper();
        List<AnswerWrapper> answers = new ArrayList<>();
        boolean isEmpty = false;

        // set question
        String value = mQuestionInput.getValue();
        question.setQuestion(value);
        isEmpty |= value.isEmpty();

        // set answer
        value = mAnswerInput.getValue();
        answers.add(new AnswerWrapper(value));
        question.setAnswers(answers);
        isEmpty |= value.isEmpty();

        // set suite
        value = mSuiteInput.getValue();
        if (value.isEmpty())
            isEmpty = true;
        else {
            Integer id = mSuites.get(value);
            if (id != null)
                question.setSuiteId(mSuites.get(value));
            else
                isEmpty = true;
        }

        // check if empty
        if (isEmpty) return null;

        // set hint
        question.setHint(mHintInput.getValue());

        return question;
    }
}
