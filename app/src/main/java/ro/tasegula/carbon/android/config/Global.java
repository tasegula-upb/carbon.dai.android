package ro.tasegula.carbon.android.config;

public final class Global {

    public static final int QUESTION_DISTANCE = 1;

    public static final class DB {
        public static final int FIRST_PAGE = 0;
        public static final int VISIBLE_THRESHOLD = 5;
        public static final int PAGE_SIZE = 30;
    }

    public static final class Pref {
        public static final String FILENAME = "ro.tasegula.carbon.android.preferences";

        public static final class Defaults {
            public static final int INT = 0;
            public static final String STR = null;
        }
    }

    public static final class ContactData {
        public static final String EMAIL = "tase92@gmail.com";

        public static final String EMAIL_EXPLAIN = "Send email ...";
        public static final String EMAIL_SUBJECT = "Carbon Feedback";
        public static final String EMAIL_BODY = "Hi Carbon,\n";

        public static final String SHARE_TEXT = "http://www.carbonapp.ro/";
        public static final String LINK = "tase92@gmail.com";
    }
}
