package ro.tasegula.carbon.android.app.survey;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import ro.tasegula.carbon.android.common.questions.SimpleQuestionView;
import ro.tasegula.carbon.android.util.Utils;

final class QuestionInteractionsSimple implements SimpleQuestionView.Interactions {

    private QuestionPresenter mPresenter;
    private SimpleQuestionView mView;

    public QuestionInteractionsSimple(QuestionPresenter presenter, SimpleQuestionView view) {
        mPresenter = presenter;
        mView = view;
    }

    @Override
    public boolean onAnswerInput(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            String answer = v.getText().toString();
            boolean correct = Utils.isCorrect(mView.getAnswer(), answer);

            mPresenter.toastQuestionResult(correct);
            mPresenter.getStats().setCorrect(correct);
            mPresenter.getStats().addAnswer(answer);

            mView.showAnswer();
            mView.clearAnswer();

            // TODO: more logic
            mPresenter.startNextQuestion();
        }
        return true;
    }
}
