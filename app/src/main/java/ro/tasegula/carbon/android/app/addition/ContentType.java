package ro.tasegula.carbon.android.app.addition;

import android.support.annotation.StringRes;

import ro.tasegula.carbon.android.R;

public enum ContentType {
    SUITE(R.string.add_suite), QUESTION(R.string.add_question), TAGS(R.string.add_tags);

    @StringRes
    public final int titleRes;

    ContentType(@StringRes int titleResource) {
        titleRes = titleResource;
    }
}
