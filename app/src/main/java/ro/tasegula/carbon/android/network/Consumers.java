package ro.tasegula.carbon.android.network;

import org.slf4j.Logger;

import io.reactivex.functions.Action;
import ro.tasegula.carbon.android.app.IMvpView;

public class Consumers {

    public static <T> SuccessConsumer<T> success(Logger logger) {
        return new SuccessConsumer<>(logger);
    }

    public static Action successCompletable(final Logger logger) {
        return new Action() {
            @Override
            public void run() {
                logger.debug("SUCCESS:");
            }
        };
    }

    public static FailureConsumer failure(Logger logger, IMvpView mvpView) {
        return new FailureConsumer(logger, mvpView);
    }

    public static FailureConsumer failure(Logger logger, IMvpView mvpView, String toast) {
        return new FailureConsumer(logger, mvpView, toast);
    }
}
