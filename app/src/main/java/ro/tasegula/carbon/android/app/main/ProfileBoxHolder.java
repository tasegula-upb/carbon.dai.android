package ro.tasegula.carbon.android.app.main;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.util.Utils;

public class ProfileBoxHolder {

    @BindView(R.id.nav_image)
    ImageView avatar;
    @BindView(R.id.nav_text)
    TextView username;
    @BindView(R.id.nav_subtext)
    TextView email;

    ProfileBoxHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }

    void setImage(Context context, String url) {
        if (!Utils.isStringEmpty(url))
            Picasso.with(context).load(url).fit().into(avatar);
    }

    void setName(String value) {
        username.setText(value);
    }

    void setEmail(String value) {
        email.setText(value);
    }
}
