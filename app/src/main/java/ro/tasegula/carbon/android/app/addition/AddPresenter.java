package ro.tasegula.carbon.android.app.addition;

import android.content.ContentResolver;
import android.net.Uri;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.AddQuestionService;
import ro.tasegula.carbon.android.rest.api.AddSuiteService;
import ro.tasegula.carbon.android.rest.api.FileUploadService;
import ro.tasegula.carbon.android.rest.api.UserService;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public class AddPresenter extends BasePresenter<AddPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(AddPresenter.class);

    @Inject
    UserService mUserService;
    @Inject
    AddQuestionService mAddQuestionService;
    @Inject
    AddSuiteService mAddSuiteService;
    @Inject
    FileUploadService mFileUploadService;

    private Uri mImageUri;
    private File mImageFile;

    @Inject
    AddPresenter() {
    }

    void setImageFile(Uri selectedImage) {
        if (selectedImage == null) return;
        mImageUri = selectedImage;
        mImageFile = getFile(selectedImage);
    }

    boolean hasImage() {
        return mImageFile != null;
    }

    void fetchEditableSuites() {
        mUserService.getEditableSuitesForId(UserData.ID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<List<SuiteWrapper>>(sLog) {
                               @Override
                               public void accept(@NonNull List<SuiteWrapper> suites) {
                                   getMvpView().updateSuitesSpinner(suites);
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    void submitQuestion(QuestionWrapper question) {
        if (question == null) return;

        Single<QuestionWrapper> request = (mImageFile != null) ?
                submitQuestionWithImage(question) :
                submitQuestionInternal(question);

        request.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<QuestionWrapper>(sLog) {
                               @Override
                               public void accept(@NonNull QuestionWrapper value) {
                                   super.accept(value);
                                   getMvpView().toast("Question Added");
                                   getMvpView().exit();
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    void submitSuite(SuiteWrapper suite) {
        if (suite == null) return;

        Single<SuiteWrapper> request = (mImageFile != null) ?
                submitSuiteWithImage(suite) :
                submitSuiteInternal(suite);

        request.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<SuiteWrapper>(sLog) {
                               @Override
                               public void accept(@NonNull SuiteWrapper value) {
                                   getMvpView().toast("Suite Added");
                                   getMvpView().exit();
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    private Single<QuestionWrapper> submitQuestionInternal(QuestionWrapper body) {
        return mAddQuestionService.addQuestion(body);
    }

    private Single<QuestionWrapper> submitQuestionWithImage(QuestionWrapper body) {
        body.setImageUri(mImageFile.getAbsolutePath());

        return mAddQuestionService.addQuestionWithImage(body,
                                                        multipartBodyPart(mImageFile),
                                                        UserData.ID);
    }

    private Single<SuiteWrapper> submitSuiteInternal(SuiteWrapper body) {
        return mAddSuiteService.addSuite(body, UserData.ID);
    }

    private Single<SuiteWrapper> submitSuiteWithImage(SuiteWrapper body) {
        body.setImageUri(mImageFile.getAbsolutePath());

        return mAddSuiteService.addSuiteImage(body,
                                              multipartBodyPart(mImageFile),
                                              UserData.ID);
    }

    private File getFile(Uri selectedImage) {
        ContentResolver contentResolver = getMvpView().context().getContentResolver();
        File file;
        try {
            InputStream in = contentResolver.openInputStream(selectedImage);
            file = File.createTempFile("image", ".png");
            FileUtils.copyToFile(in, file);
            return file;

        } catch (IOException e) {
            sLog.debug("Couldn't create file for {}", selectedImage, e);
        }

        return null;
    }

    private MultipartBody.Part multipartBodyPart(File file) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData("file", file.getName(), reqFile);
    }

    public interface View extends IMvpView {
        void updateSuitesSpinner(List<SuiteWrapper> suites);

        void exit();
    }
}
