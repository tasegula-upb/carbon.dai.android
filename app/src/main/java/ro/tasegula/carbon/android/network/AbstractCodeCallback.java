package ro.tasegula.carbon.android.network;

import org.slf4j.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractCodeCallback<T> implements Callback<T> {

    protected static final String ERROR_FORMAT = "ERROR-{}: {}";
    protected static final String RESPONSE_FORMAT = "RESPONSE-{}: {}";
    protected static final String EXCEPTION_FORMAT = "EXCEPTION: {}\n{}";

    protected abstract Logger getLogger();

    // region CODES
    protected void on200(Call<T> call, Response<T> response) {
    }

    protected void on201(Call<T> call, Response<T> response) {
    }

    protected void on404(Call<T> call, Response<T> response) {
    }

    protected void on500(Call<T> call, Response<T> response) {
    }

    protected void onException(Throwable t) {
    }
    // endregion

    @Override
    public final void onResponse(Call<T> call, Response<T> response) {
        if (!response.isSuccessful()) {
            getLogger().error(ERROR_FORMAT, response.code(), call.request().url());
        }
        getLogger().info(RESPONSE_FORMAT, response.code(), call.request().url());

        switch (response.code()) {
            case 200:
                on200(call, response);
                break;
            case 201:
                on201(call, response);
                break;
            case 404:
                on404(call, response);
                break;
            case 500:
                on500(call, response);
                break;
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        String url = call.request().url().toString();
        getLogger().error(EXCEPTION_FORMAT, url, t.getMessage());
        onException(t);
    }
}
