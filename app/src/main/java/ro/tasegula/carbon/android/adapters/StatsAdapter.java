package ro.tasegula.carbon.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ro.tasegula.carbon.android.common.cards.CardStatsView;
import ro.tasegula.carbon.android.rest.model.StatsQuestionSimpleWrapper;

public class StatsAdapter extends RecyclerView.Adapter<StatsAdapter.ViewHolder> {

    private Context mContext;
    private List<StatsQuestionSimpleWrapper> mSuites;

    public StatsAdapter(Context context, List<StatsQuestionSimpleWrapper> suites) {
        mContext = context;
        mSuites = suites;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardStatsView view = new CardStatsView(mContext);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StatsQuestionSimpleWrapper stats = mSuites.get(position);
        holder.updateCard(stats);
    }

    @Override
    public int getItemCount() {
        return mSuites.size();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View itemView) {
            super(itemView);
        }

        void updateCard(StatsQuestionSimpleWrapper stats) {
            CardStatsView view = (CardStatsView) itemView;
            view.setTitle(stats.getQuestion());
            view.setSubtitle(stats.getAnswer());
            view.setImage(stats.getImageUri());

            view.setCorrectness(stats.getData().correctness);
            view.setRating(stats.getData().ratingFull);
        }
    }
}
