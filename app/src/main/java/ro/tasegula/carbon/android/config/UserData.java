package ro.tasegula.carbon.android.config;

import android.content.SharedPreferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.rest.model.UserWrapper;
import ro.tasegula.carbon.android.util.Utils;

public class UserData {
    private static final Logger sLog = LoggerFactory.getLogger(UserData.class);

    public static int ID = 0;
    public static int collectionId = 0;
    public static String googleId = "";
    public static String googleName = "";
    public static String googleEmail = "";
    public static String googleImage = "";

    private UserData() {
    }

    public static void set(String id, String name, String email, String image) {
        UserData.googleId = id;
        UserData.googleName = name;
        UserData.googleEmail = email;
        UserData.googleImage = image;
    }

    public static void clear() {
        UserData.ID = 0;
        UserData.collectionId = 0;
        UserData.googleId = "";
        UserData.googleName = "";
        UserData.googleEmail = "";
        UserData.googleImage = "";

        Settings.reset();
    }

    public static void getFromSharedPreferences(SharedPreferences sharedPref) {
        sLog.debug("Get UserData from Preferences");
        UserData.ID = sharedPref.getInt(Fields.ID, ID);
        UserData.collectionId = sharedPref.getInt(Fields.COLLECTION_ID, collectionId);
        UserData.googleId = sharedPref.getString(Fields.GOOGLE_ID, googleId);
        UserData.googleName = sharedPref.getString(Fields.GOOGLE_NAME, googleName);
        UserData.googleEmail = sharedPref.getString(Fields.GOOGLE_EMAIL, googleEmail);
        UserData.googleImage = sharedPref.getString(Fields.GOOGLE_IMAGE, googleImage);
    }

    public static void writeToSharedPreferences(SharedPreferences sharedPref) {
        sLog.debug("Put UserData into Preferences");
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Fields.ID, ID);
        editor.putInt(Fields.COLLECTION_ID, collectionId);
        editor.putString(Fields.GOOGLE_ID, googleId);
        editor.putString(Fields.GOOGLE_NAME, googleName);
        editor.putString(Fields.GOOGLE_EMAIL, googleEmail);
        editor.putString(Fields.GOOGLE_IMAGE, googleImage);
        editor.commit();
    }

    public static boolean isLogged() {
        return ID > 0 && !Utils.isStringEmpty(googleId);
    }

    public static UserWrapper getModel() {
        return new UserWrapper(ID, googleId, googleName, googleEmail, googleImage);
    }

    public static void fromModel(UserWrapper user) {
        if (user != null) {
            UserData.ID = user.getId();
            UserData.googleId = user.getGoogleId();
            UserData.googleName = user.getGoogleName();
            UserData.googleEmail = user.getGoogleEmail();
            UserData.googleImage = user.getGoogleImage();
        } else clear();
    }

    public static String print() {
        return String.format("{%s / %s / %s}", googleId, googleName, googleImage);
    }

    static final class Fields {
        public static final String FILE = "userData";
        public static final String ID = "ID";
        public static final String COLLECTION_ID = "collectionId";
        public static final String GOOGLE_ID = "googleId";
        public static final String GOOGLE_NAME = "googleName";
        public static final String GOOGLE_EMAIL = "googleEmail";
        public static final String GOOGLE_IMAGE = "googleImage";
    }
}
