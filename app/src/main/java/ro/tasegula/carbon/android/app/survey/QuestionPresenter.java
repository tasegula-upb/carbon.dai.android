package ro.tasegula.carbon.android.app.survey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.common.questions.ComplexQuestionView;
import ro.tasegula.carbon.android.common.questions.ImageQuestionView;
import ro.tasegula.carbon.android.common.questions.SimpleQuestionView;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.QuestionService;
import ro.tasegula.carbon.android.rest.api.StatsService;
import ro.tasegula.carbon.android.rest.api.SuiteService;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.StatsPostSimpleWrapper;
import ro.tasegula.carbon.android.util.items.QuestionType;

public class QuestionPresenter extends BasePresenter<QuestionPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(QuestionPresenter.class);

    private static List<Integer> mIdList;

    private static int mSuiteId;
    private static int mCurrentId;
    private static QuestionType mQuestionType;
    private static QuestionWrapper mCurrentQuestion;

    private static StatsPostSimpleWrapper mStats;

    @Inject
    StatsService mStatsService;
    @Inject
    SuiteService mSuiteService;
    @Inject
    QuestionService mQuestionService;

    @Inject
    QuestionPresenter() {
    }

    static void updateConfiguration(QuestionWrapper questionWrapper) {
        mCurrentQuestion = questionWrapper;
        mCurrentId = (int) questionWrapper.getId();
        mQuestionType = QuestionType.getType(questionWrapper);

        if (mSuiteId != (int) questionWrapper.getSuiteId()) {
            throw new IllegalStateException(
                    "Expected state " + mSuiteId + ", got " + questionWrapper.getSuiteId());
        }
    }

    private static void assertConfiguration(int questionId, QuestionType expectedType) {
        if (mQuestionType != expectedType) {
            sLog.error("Trying to set {} question into {} holder", mQuestionType, expectedType);
        }
        if (mCurrentId != questionId) {
            sLog.error("Expected question {}, got {}", mCurrentId, questionId);
        }
    }

    void initialize(int suiteId, int questionId, QuestionType questionType) {
        mSuiteId = suiteId;
        mCurrentId = questionId;
        mQuestionType = questionType;

        mSuiteService.getQuestionIdsForId(mSuiteId)
                .flatMap(new Function<List<Integer>, SingleSource<QuestionWrapper>>() {
                    @Override
                    public SingleSource<QuestionWrapper> apply(@NonNull List<Integer> list) {
                        mIdList = list;
                        return mQuestionService.getQuestion(mCurrentId);
                    }
                })
                .flatMapCompletable(new Function<QuestionWrapper, CompletableSource>() {
                    @Override
                    public CompletableSource apply(@NonNull QuestionWrapper question) throws Exception {
                        mCurrentQuestion = question;
                        return Completable.complete();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(Consumers.successCompletable(sLog),
                           Consumers.failure(sLog, getMvpView()));
    }

    QuestionWrapper getCurrentQuestion() {
        return mCurrentQuestion;
    }

    StatsPostSimpleWrapper getStats() {
        return mStats;
    }

    private void initStats() {
        mStats = null;
        mStats = StatsPostSimpleWrapper.newStats();
        mStats.setQuestionId(mCurrentId);
    }

    void toastQuestionResult(boolean result) {
        int resultString = result ? R.string.toast_correct : R.string.toast_wrong;
        getMvpView().toast(resultString);
    }

    // ------------------------------------------------------------------------
    // region SET QUESTION DATA IN HOLDERS
    void setQuestion(int questionId, final SimpleQuestionView view) {
        assertConfiguration(questionId, QuestionType.SIMPLE);
        initStats();

        fetchQuestion(mCurrentId).subscribe(
                new SuccessConsumer<QuestionWrapper>(sLog) {
                    @Override
                    public void accept(@NonNull QuestionWrapper result) {
                        super.accept(result);

                        SimpleQuestionView.Interactions listener =
                                new QuestionInteractionsSimple(QuestionPresenter.this, view);
                        view.setListener(listener);

                        view.set(result);
                        view.clearAnswer();
                        view.showAnswerInput();
                    }
                },
                Consumers.failure(sLog, getMvpView()));
    }

    void setQuestion(int questionId, final ImageQuestionView view) {
        assertConfiguration(questionId, QuestionType.IMAGE);
        initStats();

        fetchQuestion(mCurrentId).subscribe(
                new SuccessConsumer<QuestionWrapper>(sLog) {
                    @Override
                    public void accept(@NonNull QuestionWrapper result) {
                        super.accept(result);

                        ImageQuestionView.Interactions listener =
                                new QuestionInteractionsImage(QuestionPresenter.this, view);
                        view.setListener(listener);

                        view.set(result);
                        view.clearAnswer();
                        view.showAnswerInput();
                    }
                },
                Consumers.failure(sLog, getMvpView()));
    }

    void setQuestion(int questionId, final ComplexQuestionView view) {
        assertConfiguration(questionId, QuestionType.COMPLEX);
        initStats();

        fetchQuestion(mCurrentId).subscribe(
                new SuccessConsumer<QuestionWrapper>(sLog) {
                    @Override
                    public void accept(@NonNull QuestionWrapper result) {
                        super.accept(result);

                        ComplexQuestionView.Interactions listener =
                                new QuestionInteractionsComplex(QuestionPresenter.this, view);
                        view.setListener(listener);

                        view.set(result);
                        view.clearAnswer();
                        view.showAnswerInput();
                    }
                },
                Consumers.failure(sLog, getMvpView()));
    }

    // endregion
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // region NEXT QUESTION HANDLING
    void startNextQuestion() {
        postStats();

        if (mIdList.size() == 0) {
            getMvpView().toast("Suite Done!");
            return;
        }

        mIdList.remove(Integer.valueOf(mCurrentId));
        mCurrentId = mIdList.get((new Random()).nextInt(mIdList.size()));
        mCurrentQuestion = null;

        fetchQuestion(mCurrentId).subscribe(
                new SuccessConsumer<QuestionWrapper>(sLog) {
                    @Override
                    public void accept(@NonNull QuestionWrapper questionWrapper) {
                        getMvpView().startNextQuestion(mCurrentId, mQuestionType);
                    }
                },
                Consumers.failure(sLog, getMvpView()));
    }

    private Single<QuestionWrapper> fetchQuestion(final int questionId) {
        if (questionId == mCurrentId && mCurrentQuestion != null)
            return Single.just(mCurrentQuestion);

        return mQuestionService.getQuestion(questionId)
                .doOnSuccess(new Consumer<QuestionWrapper>() {
                    @Override
                    public void accept(@NonNull QuestionWrapper questionWrapper) {
                        updateConfiguration(questionWrapper);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    private void postStats() {
        if (!mStats.isFilled()) return;

        mStatsService.postStatsForId(UserData.ID, mStats)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(Consumers.successCompletable(sLog),
                           Consumers.failure(sLog, getMvpView()));
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface View extends IMvpView {
        void startNextQuestion(int questionId, QuestionType questionType);

        void showHint();

        void showAnswer();
    }
}
