package ro.tasegula.carbon.android.app.survey;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.app.BaseActivity;
import ro.tasegula.carbon.android.dagger.Components;
import ro.tasegula.carbon.android.rest.model.SetWrapper;
import ro.tasegula.carbon.android.util.IntentHelper;

public class CategoryActivity extends BaseActivity {
    private static final Logger sLog = LoggerFactory.getLogger(CategoryActivity.class);

    public static void start(Context parent, SetWrapper set) {
        Intent intent = new Intent(parent, CategoryActivity.class);

        intent.putExtra(IntentHelper.CATEGORY_ID, (int) set.getId());
        intent.putExtra(IntentHelper.TITLE, set.getName());

        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
        setupToolbar();

        // inject
        Components.get().buildAppActivityComponent().inject(this);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        int id = intent.getIntExtra(IntentHelper.CATEGORY_ID, 0);
        String title = intent.getStringExtra(IntentHelper.TITLE);

        // Set toolbar title
        setTitle(title);

        replaceFragment(CategoryFragment.getInstance(id, title));
    }
}
