package ro.tasegula.carbon.android.app.survey;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import ro.tasegula.carbon.android.common.questions.ComplexQuestionView;
import ro.tasegula.carbon.android.util.Utils;

final class QuestionInteractionsComplex implements ComplexQuestionView.Interactions {

    private QuestionPresenter mPresenter;
    private ComplexQuestionView mView;

    private boolean mCorrect = true;

    public QuestionInteractionsComplex(QuestionPresenter presenter, ComplexQuestionView view) {
        mPresenter = presenter;
        mView = view;
    }

    @Override
    public boolean onAnswerInput(TextView v, int actionId, KeyEvent event, int position) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            String answer = v.getText().toString();
            mCorrect = mCorrect && Utils.isCorrect(mView.getAnswer(position), answer);

            mPresenter.toastQuestionResult(mCorrect);
            mPresenter.getStats().setCorrect(mCorrect);
            mPresenter.getStats().addAnswer(answer);

            mView.showAnswer();
            mView.clearAnswer();

            mPresenter.startNextQuestion();
        }
        return true;
    }
}
