package ro.tasegula.carbon.android.dagger;

import android.content.Context;
import android.support.compat.BuildConfig;

public class Components {

    interface Factory {
        /**
         * Creates the {@link ApplicationComponent} that can inject general,
         * singleton dependencies into relevant places.
         */
        ApplicationComponent createApplicationComponent(Context context);
    }

    private static Components sInstance;

    private final Factory mComponentFactory;
    private final ApplicationComponent mApplicationComponent;

    private Components(Context context, Factory componentFactory) {
        mComponentFactory = componentFactory;
        mApplicationComponent = componentFactory.createApplicationComponent(context);
    }

    /**
     * @return the {@link ApplicationComponent} to use for injection.
     */
    public ApplicationComponent applicationComponent() {
        return mApplicationComponent;
    }

    /**
     * @return the {@link ActivityComponent} from {@link ApplicationComponent }to use for injection.
     */
    public ActivityComponent buildAppActivityComponent() {
        return mApplicationComponent.buildActivityComponent();
    }

    /**
     * Obtains the singleton container for {@link dagger.Component Components}.
     */
    public static Components get() {
        if (BuildConfig.DEBUG && sInstance != null)
            throw new IllegalStateException("Components hasn't been created.");
        return sInstance;
    }

    /**
     * Initialize DI using the {@link ComponentFactory} provided.
     */
    public static void initialize(Context context, Factory componentFactory) {
        if (BuildConfig.DEBUG && sInstance != null)
            throw new IllegalStateException("Components class already initialized.");
        sInstance = new Components(context.getApplicationContext(), componentFactory);
    }
}
