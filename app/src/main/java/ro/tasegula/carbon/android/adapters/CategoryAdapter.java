package ro.tasegula.carbon.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ro.tasegula.carbon.android.common.cards.CardCategoryView;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;

public abstract class CategoryAdapter<T> extends RecyclerView.Adapter<CategoryAdapter.ViewHolder<T>> {

    private Context mContext;
    private List<T> mSuites;
    private CardCategoryView.OnClickListener<T> mListener;

    public CategoryAdapter(Context context, List<T> list,
            CardCategoryView.OnClickListener<T> listener) {
        mContext = context;
        mSuites = list;
        mListener = listener;
    }

    @Override
    public ViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        CardCategoryView view = new CardCategoryView(mContext);
        ViewHolder<T> viewHolder = new ViewHolder<>(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder<T> holder, int position) {
        T object = mSuites.get(position);
        holder.updateCard(mListener, object, getName(object));
    }

    protected abstract String getName(T object);

    @Override
    public int getItemCount() {
        return mSuites.size();
    }

    static final class ViewHolder<T> extends RecyclerView.ViewHolder {

        ViewHolder(View itemView) {
            super(itemView);
        }

        void updateCard(CardCategoryView.OnClickListener<T> listener, T suite, String name) {
            CardCategoryView<T> view = (CardCategoryView<T>) itemView;
            view.setTitle(name);

            if (listener != null)
                view.setListener(listener, suite);
        }
    }
}
