package ro.tasegula.carbon.android.app.main.statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.StatsService;
import ro.tasegula.carbon.android.rest.model.StatsQuestionSimpleWrapper;
import ro.tasegula.carbon.android.rest.model.StatsWrapper;

public class SuiteStatisticsPresenter extends BasePresenter<SuiteStatisticsPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(SuiteStatisticsPresenter.class);

    @Inject
    StatsService mStatsService;

    @Inject
    SuiteStatisticsPresenter() {
    }

    void fetchStats(int suiteId) {
        mStatsService.getUserStatsLessForSuite(UserData.ID, suiteId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<StatsWrapper<StatsQuestionSimpleWrapper>>(sLog) {
                               @Override
                               public void accept(@NonNull StatsWrapper<StatsQuestionSimpleWrapper> value) {
                                   super.accept(value);
                                   getMvpView().updateStats(value.getStats());
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    interface View extends IMvpView {
        void updateStats(List<StatsQuestionSimpleWrapper> list);
    }
}
