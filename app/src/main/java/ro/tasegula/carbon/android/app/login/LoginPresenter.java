package ro.tasegula.carbon.android.app.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.config.Settings;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.UserService;
import ro.tasegula.carbon.android.rest.model.UserWrapper;

public class LoginPresenter extends BasePresenter<LoginPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(LoginPresenter.class);

    @Inject
    UserService mUserService;

    @Inject
    SharedPreferences mPreferences;

    @Inject
    LoginPresenter() {
    }

    void signIn(GoogleSignInAccount acct) {
        UserData.googleId = acct.getId();
        UserData.googleName = acct.getDisplayName();
        UserData.googleEmail = acct.getEmail();
        UserData.googleImage = acct.getPhotoUrl().toString();

        mUserService.postUser(UserData.getModel())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<UserWrapper>(sLog) {
                               @Override
                               public void accept(@NonNull UserWrapper value) {
                                   super.accept(value);
                                   UserData.ID = value.getId();
                                   UserData.writeToSharedPreferences(mPreferences);
                                   getMvpView().update(true);
                                   Settings.reset();
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    void signOut() {
        UserData.clear();
        Settings.reset();
        UserData.writeToSharedPreferences(mPreferences);
    }

    interface View extends IMvpView {
        void update(boolean signedIn);
    }
}
