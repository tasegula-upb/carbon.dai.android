package ro.tasegula.carbon.android.app;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import ro.tasegula.carbon.android.R;
import ro.tasegula.carbon.android.dagger.ActivityComponent;
import ro.tasegula.carbon.android.dagger.Components;

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        component().injectActivity(this);
    }

    public ActivityComponent component() {
        if (mActivityComponent == null) {
            mActivityComponent = Components.get().buildAppActivityComponent();
        }
        return mActivityComponent;
    }

    public void replaceFragment(Fragment fragment) {
        int fragments = getFragmentManager().getBackStackEntryCount();

        if (fragments == 0)
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_container, fragment)
                    .commit();
        else
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_container, fragment)
                    .addToBackStack(null)
                    .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    protected Toolbar setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        return toolbar;
    }

    // region Messages
    public void toast(String text) {
        final Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_LONG);
        decorateToast(snackbar);
    }

    public void toast(@StringRes int text) {
        final Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_LONG);
        decorateToast(snackbar);
    }

    public void toastMessage(@StringRes int text) {
        final Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_INDEFINITE);
        decorateToast(snackbar);
    }

    private void decorateToast(final Snackbar snackbar) {
        snackbar.setAction(R.string.toast_dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }
    // endregion

    // ----------------------------------------------------
    // region API

//    public void showProgressWheel() {
//        progressWheel.show(getFragmentManager(), "show");
//    }
//
//    public void stopProgressWheel() {
//        progressWheel.dismiss();
//    }
    // endregion
    // ----------------------------------------------------

}
