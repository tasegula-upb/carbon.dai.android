package ro.tasegula.carbon.android.app;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ro.tasegula.carbon.android.R;

public abstract class BaseFragment extends Fragment implements IMvpView {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).component().injectFragment(this);
    }

    protected abstract void init(View rootView);

    public void setTitle(@StringRes int title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    public void pushDescriptionFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void popDescriptionFragment() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void toast(String text) {
        final Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_LONG);
        decorateToast(snackbar);
    }

    @Override
    public void toast(@StringRes int text) {
        final Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_LONG);
        decorateToast(snackbar);
    }

    public void toastMessage(@StringRes int text) {
        final Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                                                text,
                                                Snackbar.LENGTH_INDEFINITE);
        decorateToast(snackbar);
    }

    private void decorateToast(final Snackbar snackbar) {
        snackbar.setAction(R.string.toast_dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override
    public Context context() {
        return getActivity();
    }
}
