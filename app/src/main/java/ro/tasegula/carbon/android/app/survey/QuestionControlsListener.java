package ro.tasegula.carbon.android.app.survey;

import ro.tasegula.carbon.android.common.controls.ControlsView;

public class QuestionControlsListener implements ControlsView.ControlsInteractions {

    private final QuestionPresenter mPresenter;
    private final QuestionPresenter.View mMvpView;

    public QuestionControlsListener(QuestionPresenter presenter, QuestionPresenter.View mvpView) {
        mPresenter = presenter;
        mMvpView = mvpView;
    }

    @Override
    public void onHint() {
        mMvpView.showHint();
    }

    @Override
    public void onShow() {
        mMvpView.showAnswer();
    }

    @Override
    public void onSkip() {
        mPresenter.startNextQuestion();
    }

    @Override
    public void onCorrectAnswer() {
        mPresenter.getStats().setCorrect(true);
    }

    @Override
    public void onWrongAnswer() {
        mPresenter.getStats().setCorrect(false);
    }

    @Override
    public void onRating(int rating) {
        mPresenter.getStats().setRating(rating);
    }

    @Override
    public void onFinished() {
        mPresenter.startNextQuestion();
    }
}
