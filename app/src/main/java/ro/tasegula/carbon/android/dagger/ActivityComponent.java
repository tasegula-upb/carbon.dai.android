package ro.tasegula.carbon.android.dagger;

import dagger.Subcomponent;
import ro.tasegula.carbon.android.BootActivity;
import ro.tasegula.carbon.android.app.BaseActivity;
import ro.tasegula.carbon.android.app.BaseFragment;
import ro.tasegula.carbon.android.app.addition.AddActivity;
import ro.tasegula.carbon.android.app.addition.AddQuestionFragment;
import ro.tasegula.carbon.android.app.addition.AddQuestionComplexFragment;
import ro.tasegula.carbon.android.app.addition.AddSuiteFragment;
import ro.tasegula.carbon.android.app.login.LoginActivity;
import ro.tasegula.carbon.android.app.main.HelpFragment;
import ro.tasegula.carbon.android.app.main.MainActivity;
import ro.tasegula.carbon.android.app.main.MainFragment;
import ro.tasegula.carbon.android.app.main.SettingsFragment;
import ro.tasegula.carbon.android.app.main.statistics.StatisticsFragment;
import ro.tasegula.carbon.android.app.main.collection.CollectionFragment;
import ro.tasegula.carbon.android.app.main.discover.DiscoverFragment;
import ro.tasegula.carbon.android.app.main.statistics.SuiteStatisticsFragment;
import ro.tasegula.carbon.android.app.survey.CategoryActivity;
import ro.tasegula.carbon.android.app.survey.CategoryFragment;
import ro.tasegula.carbon.android.app.survey.QuestionActivity;
import ro.tasegula.carbon.android.app.survey.QuestionFragment;
import ro.tasegula.carbon.android.app.survey.SuiteActivity;
import ro.tasegula.carbon.android.app.survey.SuiteFragment;

@Subcomponent
public interface ActivityComponent {

    void injectActivity(BaseActivity activity);

    void injectFragment(BaseFragment fragment);

    void inject(BootActivity bootActivity);

    void inject(MainActivity mainActivity);

    void inject(MainFragment fragment);

    void inject(CollectionFragment fragment);

    void inject(DiscoverFragment fragment);

    void inject(StatisticsFragment fragment);

    void inject(SuiteStatisticsFragment suiteStatisticsFragment);

    void inject(HelpFragment fragment);

    void inject(SettingsFragment fragment);

    void inject(CategoryActivity categoryActivity);

    void inject(CategoryFragment fragment);

    void inject(SuiteActivity suiteActivity);

    void inject(SuiteFragment fragment);

    void inject(QuestionActivity questionActivity);

    void inject(QuestionFragment questionFragment);

    void inject(AddActivity addActivity);

    void inject(AddQuestionComplexFragment fragment);

    void inject(AddQuestionFragment fragment);

    void inject(AddSuiteFragment fragment);

    void inject(LoginActivity loginActivity);
}
