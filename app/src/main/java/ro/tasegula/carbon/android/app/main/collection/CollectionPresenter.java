package ro.tasegula.carbon.android.app.main.collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ro.tasegula.carbon.android.app.BasePresenter;
import ro.tasegula.carbon.android.app.IMvpView;
import ro.tasegula.carbon.android.config.UserData;
import ro.tasegula.carbon.android.network.Consumers;
import ro.tasegula.carbon.android.network.SuccessConsumer;
import ro.tasegula.carbon.android.rest.api.QuestionService;
import ro.tasegula.carbon.android.rest.api.SuiteService;
import ro.tasegula.carbon.android.rest.api.UserService;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;
import ro.tasegula.carbon.android.rest.model.SuiteWrapper;
import ro.tasegula.carbon.android.common.cards.CardSuitesView;

public class CollectionPresenter extends BasePresenter<CollectionPresenter.View> {
    private static final Logger sLog = LoggerFactory.getLogger(CollectionPresenter.class);

    @Inject
    UserService mUserService;
    @Inject
    SuiteService mSuiteService;
    @Inject
    QuestionService mQuestionService;

    @Inject
    CollectionPresenter() {
    }

    void fetchSuitesForUser() {
        mUserService.getSuitesForId(UserData.ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<SuiteWrapper>>() {
                               @Override
                               public void accept(@NonNull List<SuiteWrapper> list) throws Exception {
                                   getMvpView().updateSuitesList(list);
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    public CardSuitesView.OnClickListener GetCardListener() {
        return new OnClickCard(this);
    }

    private void addSuiteToUser(SuiteWrapper suite) {
        mUserService.postCopySuite(UserData.ID, suite.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(Consumers.success(sLog),
                           Consumers.failure(sLog, getMvpView()));
    }

    private void startSuite(SuiteWrapper suite) {
        sLog.debug("Start suite: {}", suite.getName());

        mSuiteService.getQuestionIdsForId((int) suite.getId())
                .flatMap(new Function<List<Integer>, SingleSource<QuestionWrapper>>() {
                    @Override
                    public SingleSource<QuestionWrapper> apply(@NonNull List<Integer> responseList) {

                        if (responseList.size() == 0) {
                            getMvpView().toast("EMPTY SUITE");
                            return null;
                        }

                        return mQuestionService.getQuestion(responseList.get(0));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SuccessConsumer<QuestionWrapper>(sLog) {
                               @Override
                               public void accept(@NonNull QuestionWrapper question) {
                                   super.accept(question);
                                   getMvpView().startQuestionActivity(question);
                               }
                           },
                           Consumers.failure(sLog, getMvpView()));
    }

    private void showSuite(SuiteWrapper suite) {
        sLog.debug("Show suite: {}", suite.getName());
        getMvpView().startSuiteActivity(suite);
    }

    // ------------------------------------------------------------------------
    // region Card#OnClickListener
    private static final class OnClickCard implements CardSuitesView.OnClickListener<SuiteWrapper> {

        private final CollectionPresenter mPresenter;

        private OnClickCard(CollectionPresenter presenter) {
            mPresenter = presenter;
        }

        @Override
        public void onAddClicked(SuiteWrapper suite) {
            mPresenter.addSuiteToUser(suite);
        }

        public void onStartClicked(SuiteWrapper suite) {
            mPresenter.startSuite(suite);
        }

        @Override
        public void onShowClicked(SuiteWrapper suite) {
            mPresenter.showSuite(suite);
        }
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface View extends IMvpView {
        void updateSuitesList(List<SuiteWrapper> list);

        void startSuiteActivity(SuiteWrapper suite);

        void startQuestionActivity(QuestionWrapper question);
    }
}
