package ro.tasegula.carbon.android.common.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import ro.tasegula.carbon.android.rest.utils.URLService;

public class ImageHelper {

    private static final String BASE_URL = URLService.IMAGE_BASE_URL;

    private final Picasso mPicasso;
    private RequestCreator mRequestCreator;

    private View mPaletteView;

    private ImageHelper(Context context) {
        mPicasso = Picasso.with(context);
        init();
    }

    private void init() {
        mRequestCreator = null;
        mPaletteView = null;
    }

    public static ImageHelper get(Context context) {
        return new ImageHelper(context);
    }

    public ImageHelper withPalette(View view) {
        mPaletteView = view;
        return this;
    }

    public ImageHelper load(Uri uri) {
        mRequestCreator = mPicasso.load(uri);
        return this;
    }

    public ImageHelper load(String partialUrl) {
        mRequestCreator = mPicasso.load(toUrl(partialUrl));
        return this;
    }

    public void into(ImageView imageView) {
        if (mPaletteView == null) {
            mRequestCreator.into(imageView);
            init();
        }
        else intoWithPalette(imageView);
    }

    private void intoWithPalette(final ImageView imageView) {
        mRequestCreator.into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                imageView.setImageBitmap(bitmap);
                PaletteHelper.get().from(bitmap).into(mPaletteView);
                init();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    private static String toUrl(String URL) {
        return BASE_URL + URL;
    }
}
