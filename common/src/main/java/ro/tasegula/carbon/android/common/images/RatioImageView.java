package ro.tasegula.carbon.android.common.images;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.common.R;

@SuppressWarnings("WeakerAccess")
public class RatioImageView extends AppCompatImageView {
	private static final Logger sLog = LoggerFactory.getLogger(RatioImageView.class);

	private float ratio;
	private int ratio_width, ratio_height;

	public RatioImageView(Context context) {
		this(context, null);
	}

	public RatioImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public RatioImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attributeSet) {
		TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.RatioView, 0, 0);

		ratio_width  = attr.getInt(R.styleable.RatioView_ratio_width, 0);
		ratio_height = attr.getInt(R.styleable.RatioView_ratio_height, 0);
		ratio        = attr.getFloat(R.styleable.RatioView_ratio, 0);

		attr.recycle();
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		int h = getMeasuredHeight();
		int w = getMeasuredWidth();

		// test if ratio needs to be applied
		// force the desired aspect ratio on the view (not the drawable).
		if (ratio_width > 0 && ratio_height > 0) {
			h = Math.round(w * ratio_width / ratio_height);
		}
		else if (ratio > 0) {
			h = Math.round(w * ratio);
		}

		setMeasuredDimension(w, h);
	}
}
