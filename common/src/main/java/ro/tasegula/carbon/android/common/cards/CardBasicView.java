package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class CardBasicView extends AbstractCardView {

    @BindView(R2.id.card_title)
    TextView mTitle;
    @BindView(R2.id.card_subtitle)
    TextView mSubtitle;

    private OnClickListener mListener;

    public CardBasicView(Context context) {
        super(context);
    }

    public CardBasicView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardBasicView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        inflate(getContext(), R.layout.view_card_basic, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Cards,
                                                               defStyleAttr,
                                                               0);
            setTitle(a.getString(R.styleable.Cards_titleText));
            setSubtitle(a.getString(R.styleable.Cards_subtitleText));

            a.recycle();
        }
    }

    @OnClick
    public void OnClick() {
        if (mListener != null)
            mListener.onView();
    }

    // ------------------------------------------------------------------------
    // region API
    public void setListener(OnClickListener listener) {
        mListener = listener;
    }

    public void setTitle(@StringRes int value) {
        mTitle.setText(value);
    }

    public void setTitle(String value) {
        mTitle.setText(value);
    }

    public void setSubtitle(@StringRes int value) {
        mSubtitle.setText(value);
    }

    public void setSubtitle(String value) {
        mSubtitle.setText(value);
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface OnClickListener {
        void onView();
    }
}
