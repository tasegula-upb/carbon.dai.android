package ro.tasegula.carbon.android.common.questions;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;

public abstract class BaseQuestionView<T> extends LinearLayout {

    @BindView(R2.id.question_title)
    TextView mQuestion;

    String mHint = null;
    T mListener;

    public BaseQuestionView(Context context) {
        this(context, null, 0);
    }

    public BaseQuestionView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);

        if (attrs == null)
            setDefaultLayoutParams();
    }

    private void setDefaultLayoutParams() {
        setOrientation(VERTICAL);
    }

    protected abstract void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr);

    // ------------------------------------------------------------------------
    // region API
    public abstract void set(QuestionWrapper question);

    public abstract void setListener(T listener);

    public void setQuestion(String value) {
        mQuestion.setText(value);
    }

    public void setHint(String value) {
        mHint = value;
    }

    public void showHint(Activity context) {
        Snackbar.make(context.findViewById(android.R.id.content),
                      mHint,
                      Snackbar.LENGTH_LONG)
                .show();
    }

    public abstract void clearAnswer();

    public abstract void showAnswer();

    public abstract void showAnswerInput();
    // endregion
    // ------------------------------------------------------------------------

    public void show() {
        setVisibility(View.VISIBLE);
    }

    public void hide() {
        setVisibility(View.GONE);
    }
}
