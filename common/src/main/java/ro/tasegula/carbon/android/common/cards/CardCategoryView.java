package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.common.utils.ColorHelper;
import ro.tasegula.carbon.android.common.utils.ImageHelper;

public class CardCategoryView<T> extends AbstractCardView {

    @BindView(R2.id.card_title)
    TextView mTitle;
    @BindView(R2.id.card_description)
    TextView mAction;
    @BindView(R2.id.card_padding)
    View mHeader;

    private OnClickListener<T> mListener;
    private ImageHelper mImageHelper;

    public CardCategoryView(Context context) {
        super(context);
    }

    public CardCategoryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardCategoryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        mImageHelper = ImageHelper.get(context);

        inflate(getContext(), R.layout.view_card_text_padding, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Cards,
                                                               defStyleAttr,
                                                               0);
            setTitle(a.getString(R.styleable.Cards_titleText));
            setAction(a.getString(R.styleable.Cards_actionText));

            a.recycle();
        }

        setColor(ColorHelper.get(getContext()).getColor());
    }

    // ------------------------------------------------------------------------
    // region API
    public void setListener(final OnClickListener listener, final T object) {
        mListener = listener;

        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onView(object);
            }
        });
    }

    public void setTitle(@StringRes int value) {
        mTitle.setText(value);
    }

    public void setTitle(String value) {
        mTitle.setText(value);
    }

    public void setColor(@ColorInt int color) {
        mHeader.setBackgroundColor(color);
        mAction.setBackgroundColor(color);
    }

    public void setAction(@StringRes int value) {
        mAction.setText(value);
    }

    public void setAction(String value) {
        mAction.setText(value);
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface OnClickListener<T> {
        void onView(T object);
    }
}
