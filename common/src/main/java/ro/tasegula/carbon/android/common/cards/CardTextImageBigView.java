package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.common.images.RatioImageView;
import ro.tasegula.carbon.android.common.utils.ImageHelper;

public class CardTextImageBigView extends AbstractCardView {

    @BindView(R2.id.card_title)
    TextView mTitle;
    @BindView(R2.id.card_image)
    RatioImageView mImage;

    private OnClickListener mListener;
    private ImageHelper mImageHelper;

    public CardTextImageBigView(Context context) {
        super(context);
    }

    public CardTextImageBigView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardTextImageBigView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        mImageHelper = ImageHelper.get(context);

        inflate(getContext(), R.layout.view_card_basic, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Cards,
                                                               defStyleAttr,
                                                               0);
            setTitle(a.getString(R.styleable.Cards_titleText));
            setImage(a.getDrawable(R.styleable.Cards_imageSrc));

            a.recycle();
        }
    }

    @OnClick
    public void OnClick() {
        if (mListener != null)
            mListener.onView();
    }

    // ------------------------------------------------------------------------
    // region API
    public void setListener(OnClickListener listener) {
        mListener = listener;
    }

    public void setTitle(@StringRes int value) {
        mTitle.setText(value);
    }

    public void setTitle(String value) {
        mTitle.setText(value);
    }

    public void setImage(@DrawableRes int value) {
        mImage.setImageResource(value);
    }

    public void setImage(Drawable value) {
        mImage.setImageDrawable(value);
    }

    public void setImage(String url) {
        mImageHelper.load(url).withPalette(this).into(mImage);
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface OnClickListener {
        void onView();
    }
}
