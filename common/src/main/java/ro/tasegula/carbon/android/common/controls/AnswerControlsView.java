package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class AnswerControlsView extends AbstractControlsView<AnswerControlsView.OnClick> {

    @BindView(R2.id.controlsA_correct)
    Button mCorrect;
    @BindView(R2.id.controlsA_wrong)
    Button mWrong;

    public AnswerControlsView(Context context) {
        super(context);
    }

    public AnswerControlsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AnswerControlsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        inflate(getContext(), R.layout.view_controls_answer, this);
        ButterKnife.bind(this);

        mCorrect.setOnClickListener(this);
        mWrong.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) return;

        int id = v.getId();
        if (id == R.id.controlsA_correct) {
            mListener.onCorrect();

        } else if (id == R.id.controlsA_wrong) {
            mListener.onWrong();

        }
    }

    public interface OnClick {
        void onCorrect();

        void onWrong();
    }
}
