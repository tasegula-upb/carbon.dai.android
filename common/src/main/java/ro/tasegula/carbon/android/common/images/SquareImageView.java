package ro.tasegula.carbon.android.common.images;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.tasegula.carbon.android.common.R;

public class SquareImageView extends AppCompatImageView {
    private static final Logger sLog = LoggerFactory.getLogger(SquareImageView.class);

    private int type = ImageUtils.TakeBy.SMALL;

    public SquareImageView(Context context) {
        this(context, null);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        TypedArray attr = context.obtainStyledAttributes(attributeSet, R.styleable.RatioView, 0, 0);

        type = attr.getInt(R.styleable.SquareView_take, ImageUtils.TakeBy.WIDTH);

        attr.recycle();
        sLog.debug("TYPE: {}", type);
    }

    private int squareDim = Integer.MAX_VALUE;

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        sLog.debug("param: {} / {}", widthMeasureSpec, heightMeasureSpec);

        int w = this.getMeasuredWidth();
        int h = this.getMeasuredHeight();
        sLog.debug("measured: {} / {}", w, h);

        int curSquareDim = (type == ImageUtils.TakeBy.SMALL) ? Math.min(w, h) :
                (type == ImageUtils.TakeBy.WIDTH) ? w :
                        (type == ImageUtils.TakeBy.BIG) ? Math.max(w, h) :
                                (type == ImageUtils.TakeBy.HEIGHT) ? h :
                                        Math.min(w, h);

        sLog.debug("cur: {}", curSquareDim);
        sLog.debug("squared: {}", squareDim);
        if (curSquareDim < squareDim) {
            squareDim = curSquareDim;
            sLog.debug("curIn: {}", curSquareDim);
            sLog.debug("squaredIn: {}", squareDim);
        }

        sLog.debug("squaredOut: {}", squareDim);
        setMeasuredDimension(squareDim, squareDim);
    }

}
