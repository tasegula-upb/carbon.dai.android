package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class ControlsView extends FrameLayout implements QuestionControlsView.OnClick,
        RatingControlsView.OnClick, AnswerControlsView.OnClick {

    @BindView(R2.id.controls_question)
    QuestionControlsView mQuestionControls;
    @BindView(R2.id.controls_answer)
    AnswerControlsView mAnswerControls;

    RatingControlsView mRatingControls;

    private boolean mShowCorrectness;
    private boolean mShowRating;
    private ControlsInteractions mListener;

    public ControlsView(@NonNull Context context) {
        this(context, null, 0);
    }

    public ControlsView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ControlsView(@NonNull Context context, @Nullable AttributeSet attrs,
            @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(@Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        inflate(getContext(), R.layout.view_controls, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Controls,
                                                               defStyleAttr,
                                                               0);

            mShowRating = a.getBoolean(R.styleable.Controls_showRating, true);
            mShowCorrectness = a.getBoolean(R.styleable.Controls_showRating, true);
            int ratingType = a.getInt(R.styleable.Controls_ratingType, 0);

            if (mShowRating) {
                if (ratingType == 0)
                    mRatingControls = new RatingNumberControlsView(getContext());
                else if (ratingType == 1)
                    mRatingControls = new RatingTextControlsView(getContext());
                addView(mRatingControls);
            }

            a.recycle();
        }

        mQuestionControls.setListener(this);
        mAnswerControls.setListener(this);
        if (mRatingControls != null)
            mRatingControls.setListener(this);

        mQuestionControls.show();
        mAnswerControls.hide();
        if (mRatingControls != null)
            mRatingControls.hide();
    }

    public void setListener(ControlsInteractions listener) {
        mListener = listener;
    }

    public void setRatingType(@NonNull RatingType type) {
        removeView(mRatingControls);
        mRatingControls = null;

        switch (type) {
            case NUMBER:
                mRatingControls = new RatingNumberControlsView(getContext());
                break;
            case TEXT:
                mRatingControls = new RatingTextControlsView(getContext());
                break;
        }

        if (mRatingControls != null) {
            addView(mRatingControls);
            mRatingControls.hide();
            mRatingControls.setListener(this);
        }

        mShowRating = (type != RatingType.NONE);
    }

    public void setShowCorrectness(boolean value) {
        mShowCorrectness = value;
    }

    // region LISTENER
    @Override
    public void onHint() {
        mListener.onHint();
    }

    @Override
    public void onShow() {
        mQuestionControls.hide();
        mListener.onShow();
        if (mShowCorrectness) mAnswerControls.show();
        else if (mShowRating) mRatingControls.show();
        else endRound();
    }

    @Override
    public void onSkip() {
        mListener.onSkip();
    }

    @Override
    public void onCorrect() {
        mAnswerControls.hide();
        mListener.onCorrectAnswer();
        if (mRatingControls != null) mRatingControls.show();
        else endRound();
    }

    @Override
    public void onWrong() {
        mAnswerControls.hide();
        mListener.onWrongAnswer();
        if (mRatingControls != null) mRatingControls.show();
        else endRound();
    }

    @Override
    public void onRating(int rating) {
        mRatingControls.hide();
        mListener.onRating(rating);
        endRound();
    }

    public void endRound() {
        mQuestionControls.show();
        mListener.onFinished();
    }
    // endregion

    public interface ControlsInteractions {
        void onHint();

        void onShow();

        void onSkip();

        void onCorrectAnswer();

        void onWrongAnswer();

        void onRating(int rating);

        void onFinished();
    }
}
