package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import ro.tasegula.carbon.android.common.R;

public class RatingTextControlsView extends RatingControlsView {

    public RatingTextControlsView(Context context) {
        super(context);
    }

    public RatingTextControlsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RatingTextControlsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.view_controls_rating_text;
    }
}
