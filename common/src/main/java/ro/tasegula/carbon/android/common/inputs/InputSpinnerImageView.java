package ro.tasegula.carbon.android.common.inputs;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class InputSpinnerImageView extends LinearLayout {

    @BindView(R2.id.image)
    ImageView mImage;
    @BindView(R2.id.input)
    Spinner mInput;

    public InputSpinnerImageView(@NonNull Context context) {
        this(context, null, 0);
    }

    public InputSpinnerImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InputSpinnerImageView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        inflate(getContext(), R.layout.view_input_spinner, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.InputImage,
                                                               defStyleAttr,
                                                               0);

            String hint = a.getString(R.styleable.InputImage_inputHint);
            Drawable icon = a.getDrawable(R.styleable.InputImage_inputIcon);

            mImage.setImageDrawable(icon);
            mInput.setPrompt(hint);

            a.recycle();
        }
    }

    @NonNull
    public String getValue() {
        Object value = mInput.getSelectedItem();
        if (value == null) {
            mInput.setBackgroundColor(Color.RED);
            return "";
        }
        return value.toString();
    }

    public void setAdapter(ArrayAdapter<String> adapter) {
        mInput.setAdapter(adapter);
    }

    public void setListener(AdapterView.OnItemSelectedListener listener) {
        mInput.setOnItemSelectedListener(listener);
    }
}
