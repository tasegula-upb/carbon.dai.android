package ro.tasegula.carbon.android.common.fab;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class FabMenuItem extends LinearLayout {

    @BindView(R2.id.item_label)
    TextView mLabel;
    @BindView(R2.id.item_fab)
    FloatingActionButton mFab;

    public FabMenuItem(Context context) {
        this(context, null, 0);
    }

    public FabMenuItem(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FabMenuItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);

        if (attrs == null)
            setDefaultLayoutParams();
    }

    protected void setDefaultLayoutParams() {
        int margin = getContext().getResources().getDimensionPixelOffset(R.dimen.fab_small_margin);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.BOTTOM | Gravity.END;

        layoutParams.setMargins(0, 0, margin, margin);
        setLayoutParams(layoutParams);
    }

    protected void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        inflate(context, R.layout.view_fab_menu_item, this);
        ButterKnife.bind(this);

        Resources resources = context.getResources();

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.FabMenuItem,
                                                               defStyleAttr,
                                                               0);

            String labelText = a.getString(R.styleable.FabMenuItem_labelText);
            int labelColor = a.getColor(R.styleable.FabMenuItem_labelBackground, Color.TRANSPARENT);
            int labelTextColor = a.getColor(R.styleable.FabMenuItem_labelColor,
                                            resources.getColor(R.color.colorTextPrimary));

            Drawable fabIcon = a.getDrawable(R.styleable.FabMenuItem_fabIcon);
            int fabColor = a.getColor(R.styleable.FabMenuItem_fabColor, resources.getColor(R.color.colorAccent));
            int fabRippleColor = a.getColor(R.styleable.FabMenuItem_fabRippleColor,
                                            resources.getColor(R.color.colorAccent));

            mLabel.setText(labelText);
            mLabel.setTextColor(labelTextColor);
            if (labelColor != Color.TRANSPARENT)
                mLabel.setBackgroundColor(labelColor);

            mFab.setImageDrawable(fabIcon);
            mFab.setBackgroundColor(fabColor);
            mFab.setRippleColor(fabRippleColor);

            a.recycle();
        }
    }

    public void show() {
        setVisibility(View.VISIBLE);
    }

    public void hide() {
        setVisibility(View.GONE);
    }
}
