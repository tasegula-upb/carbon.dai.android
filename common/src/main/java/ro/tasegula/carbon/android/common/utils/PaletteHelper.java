package ro.tasegula.carbon.android.common.utils;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.support.v7.graphics.Target;
import android.view.View;
import android.widget.ImageView;

import java.util.SortedMap;
import java.util.TreeMap;

public class PaletteHelper {

    private Target mTarget;
    private Bitmap mBitmap;

    private PaletteHelper() {
        init();
    }

    private void init() {
        mBitmap = null;
        mTarget = null;
    }

    public static PaletteHelper get() {
        return new PaletteHelper();
    }
    
    public PaletteHelper from(Bitmap bitmap) {
        mBitmap = bitmap;
        return this;
    }

    public PaletteHelper withTarget(Target target) {
        mTarget = target;
        return this;
    }
    
    public void into(View view) {
        if (mTarget == null) addBiggestPalette(view);
        else addTargetPalette(view);
    }

    private void addBiggestPalette(final View view) {
        Palette.from(mBitmap).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                Palette.Swatch swatch = getBiggestPopulation(palette);
                if (swatch != null)
                    view.setBackgroundColor(swatch.getRgb() | 0xFF000000);
                init();
            }
        });
    }

    private void addTargetPalette(final View view) {
        Palette.from(mBitmap).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                Palette.Swatch swatch = palette.getSwatchForTarget(mTarget);
                if (swatch != null)
                    view.setBackgroundColor(swatch.getRgb() | 0xFF000000);
                init();
            }
        });
    }

    @Nullable
    private static Palette.Swatch getBiggestPopulation(Palette palette) {
        Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
        Palette.Swatch vibrantLightSwatch = palette.getLightVibrantSwatch();
        Palette.Swatch vibrantDarkSwatch = palette.getDarkVibrantSwatch();
        Palette.Swatch mutedSwatch = palette.getMutedSwatch();
        Palette.Swatch mutedLightSwatch = palette.getLightMutedSwatch();
        Palette.Swatch mutedDarkSwatch = palette.getDarkMutedSwatch();

        SortedMap<Integer, Palette.Swatch> map = new TreeMap<>();
        put(map, vibrantSwatch);
        put(map, vibrantLightSwatch);
        put(map, vibrantDarkSwatch);
        put(map, mutedSwatch);
        put(map, mutedLightSwatch);
        put(map, mutedDarkSwatch);

        if (map.size() == 0) return null;
        return map.get(map.lastKey());
    }

    private static void put(SortedMap<Integer, Palette.Swatch> map, Palette.Swatch swatch) {
        if (swatch != null) {
            map.put(swatch.getPopulation(), swatch);
        }
    }
}
