package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class QuestionControlsView extends AbstractControlsView<QuestionControlsView.OnClick> {

    @BindView(R2.id.controlsQ_hint)
    Button mHint;
    @BindView(R2.id.controlsQ_show)
    Button mShow;
    @BindView(R2.id.controlsQ_skip)
    Button mSkip;

    public QuestionControlsView(Context context) {
        super(context);
    }

    public QuestionControlsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public QuestionControlsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        inflate(getContext(), R.layout.view_controls_question, this);
        ButterKnife.bind(this);

        mHint.setOnClickListener(this);
        mShow.setOnClickListener(this);
        mSkip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) return;

        int id = v.getId();
        if (id == R.id.controlsQ_hint) {
            mListener.onHint();

        } else if (id == R.id.controlsQ_show) {
            mListener.onShow();

        } else if (id == R.id.controlsQ_skip) {
            mListener.onSkip();

        }
    }

    public interface OnClick {
        void onHint();

        void onShow();

        void onSkip();
    }
}
