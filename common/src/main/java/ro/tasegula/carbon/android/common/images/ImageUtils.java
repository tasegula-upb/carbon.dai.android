package ro.tasegula.carbon.android.common.images;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ImageUtils {

	// -------------------------------------------------------------------------
	// REQUEST INTERFACES
	// -------------------------------------------------------------------------

	@Retention(RetentionPolicy.SOURCE)
	@IntDef({ TakeBy.WIDTH, TakeBy.HEIGHT, TakeBy.SMALL, TakeBy.BIG})
	@interface ReqType {
	}

	// -------------------------------------------------------------------------
	// GENERAL PROPERTIES - BUTTON
	// -------------------------------------------------------------------------

	public static final class TakeBy {
		static final int WIDTH  = 0;
		static final int HEIGHT = 1;
		static final int SMALL  = 2;
		static final int BIG    = 3;
	}

}
