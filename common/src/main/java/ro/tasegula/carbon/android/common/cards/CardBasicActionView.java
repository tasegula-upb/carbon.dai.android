package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;

public class CardBasicActionView extends AbstractCardView {

    @BindView(R2.id.card_title)
    TextView mTitle;
    @BindView(R2.id.card_subtitle)
    TextView mSubtitle;
    @BindView(R2.id.card_action)
    Button mAction;

    private OnClickListener mListener;

    public CardBasicActionView(Context context) {
        super(context);
    }

    public CardBasicActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardBasicActionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        inflate(getContext(), R.layout.view_card_basic_action, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Cards,
                                                               defStyleAttr,
                                                               0);
            setTitle(a.getString(R.styleable.Cards_titleText));
            setSubtitle(a.getString(R.styleable.Cards_subtitleText));
            setActionText(a.getString(R.styleable.Cards_actionText));

            a.recycle();
        }
    }

    // ------------------------------------------------------------------------
    // region API
    public void setTitle(@StringRes int value) {
        mTitle.setText(value);
    }

    public void setTitle(String value) {
        mTitle.setText(value);
    }

    public void setSubtitle(@StringRes int value) {
        mSubtitle.setText(value);
    }

    public void setSubtitle(String value) {
        mSubtitle.setText(value);
    }

    public void setActionText(@StringRes int value) {
        mAction.setText(value);
    }

    public void setActionText(String value) {
        mAction.setText(value);
    }

    public void setAction(final OnClickListener listener) {
        mListener = listener;
        mAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAction();
            }
        });
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface OnClickListener {
        void onAction();
    }
}
