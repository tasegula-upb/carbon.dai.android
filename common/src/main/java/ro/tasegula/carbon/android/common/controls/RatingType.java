package ro.tasegula.carbon.android.common.controls;

public enum RatingType {
    NUMBER, TEXT, NONE;

    public static RatingType from(int type) {
        switch (type) {
            case 0:
                return NUMBER;
            case 1:
                return TEXT;
            default:
                return NONE;
        }
    }

    public static RatingType from(String type) {
        if (type == null)
            return NONE;

        if (type.equals(NUMBER.name())) return NUMBER;
        if (type.equals(TEXT.name())) return TEXT;
        return NONE;
    }

    public static RatingType fromPreference(String type) {
        if (type == null)
            return NONE;

        switch (type) {
            case "0":
                return NUMBER;
            case "1":
                return TEXT;
            default:
                return NONE;
        }
    }
}
