package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;

public abstract class RatingControlsView extends AbstractControlsView<RatingControlsView.OnClick> {

    private enum Rating {
        LVL0(R.id.controlsR_lvl0),
        LVL1(R.id.controlsR_lvl1),
        LVL2(R.id.controlsR_lvl2),
        LVL3(R.id.controlsR_lvl3),
        LVL4(R.id.controlsR_lvl4);

        @IdRes
        int resourceId;

        Rating(@IdRes int id) {
            resourceId = id;
        }
    }

    public RatingControlsView(Context context) {
        super(context);
    }

    public RatingControlsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RatingControlsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        inflate(getContext(), getLayoutResource(), this);
        ButterKnife.bind(this);

        for (Rating rating : Rating.values()) {
            findViewById(rating.resourceId).setOnClickListener(this);
        }
    }

    protected abstract
    @LayoutRes
    int getLayoutResource();

    @Override
    public void onClick(View v) {
        if (mListener == null) return;

        int id = v.getId();
        if (id == R.id.controlsR_lvl0) {
            mListener.onRating(0);

        } else if (id == R.id.controlsR_lvl1) {
            mListener.onRating(1);

        } else if (id == R.id.controlsR_lvl2) {
            mListener.onRating(2);

        } else if (id == R.id.controlsR_lvl3) {
            mListener.onRating(3);

        } else if (id == R.id.controlsR_lvl4) {
            mListener.onRating(4);
        }
    }

    public interface OnClick {
        void onRating(int rating);
    }
}
