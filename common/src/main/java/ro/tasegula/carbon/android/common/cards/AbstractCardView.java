package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Px;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ro.tasegula.carbon.android.common.R;

public abstract class AbstractCardView extends CardView {

    public AbstractCardView(Context context) {
        this(context, null, 0);
    }

    public AbstractCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AbstractCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);

        if (attrs == null)
            setDefaultLayoutParams();
    }

    protected abstract void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr);

    protected void initText(TextView view, @Nullable String what) {
        if (what != null) view.setText(what);
    }

    protected void initButton(Button view, @Nullable String what) {
        if (what != null) view.setText(what);
    }

    protected void initImage(ImageView view, @Nullable Drawable what) {
        if (what != null) view.setImageDrawable(what);
    }

    protected
    @Px
    int getDefaultMargin() {
        return getContext().getResources().getDimensionPixelOffset(R.dimen.marginSmallX);
    }

    protected
    @Px
    int getDefaultElevation() {
        return getContext().getResources().getDimensionPixelOffset(R.dimen.elevationSmallX);
    }

    protected void setDefaultLayoutParams() {
        int margin = getDefaultMargin();
        MarginLayoutParams layoutParams = new MarginLayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(margin, margin, margin, 0);
        setLayoutParams(layoutParams);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(getDefaultElevation());
        }
    }
}
