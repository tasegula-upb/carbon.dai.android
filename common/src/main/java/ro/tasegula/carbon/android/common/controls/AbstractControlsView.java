package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import ro.tasegula.carbon.android.common.R;

public abstract class AbstractControlsView<T> extends LinearLayout implements View.OnClickListener {

    protected T mListener;

    public AbstractControlsView(Context context) {
        this(context, null, 0);
    }

    public AbstractControlsView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AbstractControlsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

        if (attrs == null)
            setDefaultLayoutParams();

        setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    private void setDefaultLayoutParams() {
        int margin = getContext().getResources().getDimensionPixelOffset(R.dimen.controlMargin);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.BOTTOM;

        layoutParams.setMargins(0, margin, 0, 0);
        setLayoutParams(layoutParams);
    }

    protected abstract void init();

    public void setListener(T listener) {
        mListener = listener;
    }

    public void show() {
        setVisibility(View.VISIBLE);
    }

    public void hide() {
        setVisibility(View.GONE);
    }
}
