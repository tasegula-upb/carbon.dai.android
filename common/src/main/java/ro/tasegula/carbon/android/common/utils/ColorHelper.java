package ro.tasegula.carbon.android.common.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ColorInt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ro.tasegula.carbon.android.common.R;

public class ColorHelper {

    private static ColorHelper INSTANCE;

    public int[] sColorIds = {
            R.color.mr_violet,
            R.color.mr_red,
            R.color.mr_orange,
            R.color.mr_yellow,
            R.color.mr_lime,
            R.color.mr_green,
            R.color.mr_teal,
            R.color.mr_blue,
            R.color.mr_indigo,
            R.color.mr_purple,
    };
    private final int mSize = sColorIds.length;

    private final List<Integer> mColors;
    private final Random mRandom;

    private ColorHelper(Context context) {
        Resources resources = context.getResources();
        mRandom = new Random();
        mColors = new ArrayList<>(10);

        for (int colorId : sColorIds) {
            mColors.add(resources.getColor(colorId));
        }
    }

    public static ColorHelper get(Context context) {
        if (INSTANCE == null)
            INSTANCE = new ColorHelper(context);
        return INSTANCE;
    }

    public
    @ColorInt
    int getColor(int position) {
        if (position < 0 || position < mSize)
            return getColor();

        return mColors.get(position);
    }

    public
    @ColorInt
    int getColor() {
        return mColors.get(mRandom.nextInt(mSize));
    }
}
