package ro.tasegula.carbon.android.common.questions;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.common.utils.ImageHelper;
import ro.tasegula.carbon.android.rest.model.AnswerWrapper;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;

public class ComplexQuestionView extends BaseQuestionView<ComplexQuestionView.Interactions> {
private static final Logger sLog = LoggerFactory.getLogger(SimpleQuestionView.class);

    @BindView(R2.id.question_image)
    ImageView image;

    private List<TextView> mAnswers;
    private List<EditText> mAnswerInputs;

    private ImageHelper mImageHelper;
    private int mSize = 5;

    public ComplexQuestionView(Context context) {
        super(context);
    }

    public ComplexQuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ComplexQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        mImageHelper = ImageHelper.get(context);
        inflate(getContext(), R.layout.view_question_complex, this);
        ButterKnife.bind(this);

        mAnswers = new ArrayList<>(mSize);
        mAnswers.add((TextView) findViewById(R.id.question_answer1));
        mAnswers.add((TextView) findViewById(R.id.question_answer2));
        mAnswers.add((TextView) findViewById(R.id.question_answer3));
        mAnswers.add((TextView) findViewById(R.id.question_answer4));
        mAnswers.add((TextView) findViewById(R.id.question_answer5));

        mAnswerInputs = new ArrayList<>(mSize);
        mAnswerInputs.add((EditText) findViewById(R.id.question_answerInput1));
        mAnswerInputs.add((EditText) findViewById(R.id.question_answerInput2));
        mAnswerInputs.add((EditText) findViewById(R.id.question_answerInput3));
        mAnswerInputs.add((EditText) findViewById(R.id.question_answerInput4));
        mAnswerInputs.add((EditText) findViewById(R.id.question_answerInput5));
    }

    // ------------------------------------------------------------------------
    // region API
    @Override
    public void set(QuestionWrapper question) {
        setQuestion(question.getQuestion());
        setImage(question.getImageUri());
        setAnswers(question.getAnswers());
        setHint(question.getHint());
    }

    @Override
    public void setListener(Interactions listener) {
        mListener = listener;
        final AtomicInteger index = new AtomicInteger(0);

        for (int i = 0; i < mSize; i++) {
            mAnswerInputs.get(i).setOnEditorActionListener(
                    new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            return mListener.onAnswerInput(v, actionId, event, index.getAndIncrement());
                        }
                    });
        }
    }

    public void setImage(String partialUri) {
        mImageHelper.load(partialUri).into(image);
    }
    public List<String> getAnswers() {
        List<String> result = new ArrayList<>(mSize);

        for (TextView answer : mAnswers) {
            result.add(answer.getText().toString());
        }

        return result;
    }

    public String getAnswer(int position) {
        return mAnswers.get(position).getText().toString();
    }

    public void setAnswers(List<AnswerWrapper> list) {
        mSize = list.size();

        for (int i = 0; i < mSize; i++) {
            AnswerWrapper answerWrapper = list.get(i);
            EditText editText = mAnswerInputs.get(i);
            TextView textView = mAnswers.get(i);

            editText.setVisibility(View.VISIBLE);
            textView.setText(answerWrapper.getAnswer());

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                                                                 ViewGroup.LayoutParams.WRAP_CONTENT);
            params.leftMargin = answerWrapper.getPointX();
            params.topMargin = answerWrapper.getPointY();

            textView.setLayoutParams(params);
            editText.setLayoutParams(params);
        }
    }

    @Override
    public void clearAnswer() {
        for (EditText edit : mAnswerInputs) {
            edit.setText("");
        }
    }

    @Override
    public void showAnswer() {
        for (int i = 0; i < mSize; i++) {
            TextView textView = mAnswers.get(i);
            EditText editText = mAnswerInputs.get(i);

            editText.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showAnswerInput() {
        for (int i = 0; i < mSize; i++) {
            TextView textView = mAnswers.get(i);
            EditText editText = mAnswerInputs.get(i);

            textView.setVisibility(View.GONE);
            editText.setVisibility(View.VISIBLE);
        }
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface Interactions {
        boolean onAnswerInput(TextView v, int actionId, KeyEvent event, int position);
    }
}
