package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.common.utils.ImageHelper;

public class CardStatsView extends AbstractCardView {

    @BindView(R2.id.card_title)
    TextView mTitle;
    @BindView(R2.id.card_subtitle)
    TextView mSubtitle;
    @BindView(R2.id.card_image)
    ImageView mImage;
    @BindView(R2.id.card_badge)
    ImageView mBadge;
    @BindView(R2.id.card_correctness)
    Button mCorrectness;
    @BindView(R2.id.card_rating)
    Button mRating;

    private ImageHelper mImageHelper;

    public CardStatsView(Context context) {
        super(context);
    }

    public CardStatsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardStatsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        mImageHelper = ImageHelper.get(context);

        inflate(getContext(), R.layout.view_card_stats, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Cards,
                                                               defStyleAttr,
                                                               0);
            initText(mTitle, a.getString(R.styleable.Cards_titleText));
            initText(mSubtitle, a.getString(R.styleable.Cards_subtitleText));
            initImage(mImage, a.getDrawable(R.styleable.Cards_imageSrc));

            a.recycle();
        }
    }

    // ------------------------------------------------------------------------
    // region API

    public void setTitle(@StringRes int value) {
        mTitle.setText(value);
    }

    public void setTitle(String value) {
        mTitle.setText(value);
    }

    public void setSubtitle(@StringRes int value) {
        mSubtitle.setText(value);
    }

    public void setSubtitle(String value) {
        mSubtitle.setText(value);
    }

    public void setImage(@DrawableRes int value) {
        mImage.setImageResource(value);
    }

    public void setImage(Drawable value) {
        mImage.setImageDrawable(value);
    }

    public void setImage(String url) {
        mImageHelper.load(url).into(mImage);
    }

    public void setRating(float value) {
        mRating.setText(String.format("%.1f", value));
    }

    public void setCorrectness(float value) {
        mCorrectness.setText(String.format("Correctness: %d%%", (int) value));
    }

    public void setBadgeValue(int value) {
        if (value < 10) return;
        else if (value < 50) mBadge.setImageResource(R.drawable.ic_badge_simple_010);
        else if (value < 100) mBadge.setImageResource(R.drawable.ic_badge_simple_050);
        else mBadge.setImageResource(R.drawable.ic_badge_simple_100);
    }

    // endregion
    // ------------------------------------------------------------------------
}
