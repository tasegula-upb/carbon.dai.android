package ro.tasegula.carbon.android.common.questions;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.common.utils.ImageHelper;
import ro.tasegula.carbon.android.rest.model.QuestionWrapper;

public class ImageQuestionView extends BaseQuestionView<ImageQuestionView.Interactions> {
private static final Logger sLog = LoggerFactory.getLogger(SimpleQuestionView.class);

    @BindView(R2.id.question_image)
    ImageView image;

    @BindView(R2.id.question_answer)
    TextView mAnswer;

    @BindView(R2.id.question_answerInput)
    EditText mAnswerInput;

    private ImageHelper mImageHelper;

    public ImageQuestionView(Context context) {
        super(context);
    }

    public ImageQuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        mImageHelper = ImageHelper.get(context);
        inflate(getContext(), R.layout.view_question_image, this);
        ButterKnife.bind(this);
    }

    // ------------------------------------------------------------------------
    // region API
    @Override
    public void set(QuestionWrapper question) {
        setQuestion(question.getQuestion());
        setImage(question.getImageUri());
        setAnswer(question.getAnswers().get(0).getAnswer());
        setHint(question.getHint());
    }

    @Override
    public void setListener(Interactions listener) {
        mListener = listener;

        mAnswerInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return mListener.onAnswerInput(v, actionId, event);
            }
        });
    }

    public void setImage(String partialUri) {
        mImageHelper.load(partialUri).into(image);
    }

    public String getAnswer() {
        return mAnswer.getText().toString();
    }

    public void setAnswer(String value) {
        mAnswer.setText(value);
    }

    @Override
    public void clearAnswer() {
        mAnswerInput.setText("");
    }

    @Override
    public void showAnswer() {
        mAnswer.setVisibility(View.VISIBLE);
        mAnswerInput.setVisibility(View.GONE);
    }

    @Override
    public void showAnswerInput() {
        mAnswer.setVisibility(View.GONE);
        mAnswerInput.setVisibility(View.VISIBLE);
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface Interactions {
        boolean onAnswerInput(TextView v, int actionId, KeyEvent event);
    }
}
