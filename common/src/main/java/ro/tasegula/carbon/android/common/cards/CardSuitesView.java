package ro.tasegula.carbon.android.common.cards;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.tasegula.carbon.android.common.R;
import ro.tasegula.carbon.android.common.R2;
import ro.tasegula.carbon.android.common.utils.ImageHelper;

public class CardSuitesView<T> extends AbstractCardView {

    @BindView(R2.id.card_title)
    TextView mTitle;
    @BindView(R2.id.card_subtitle)
    TextView mSubtitle;
    @BindView(R2.id.card_image)
    ImageView mImage;
    @BindView(R2.id.card_action0)
    Button mActionMain;
    @BindView(R2.id.card_action1)
    Button mActionLeft;
    @BindView(R2.id.card_action2)
    Button mActionRight;

    private OnClickListener<T> mListener;
    private ImageHelper mImageHelper;

    public CardSuitesView(Context context) {
        super(context);
    }

    public CardSuitesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardSuitesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        mImageHelper = ImageHelper.get(context);

        inflate(getContext(), R.layout.view_card_suites, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                                                               R.styleable.Cards,
                                                               defStyleAttr,
                                                               0);
            initText(mTitle, a.getString(R.styleable.Cards_titleText));
            initText(mSubtitle, a.getString(R.styleable.Cards_subtitleText));
            initButton(mActionMain, a.getString(R.styleable.Cards_actionText));
            initButton(mActionLeft, a.getString(R.styleable.Cards_actionLeftText));
            initButton(mActionRight, a.getString(R.styleable.Cards_actionRightText));
            initImage(mImage, a.getDrawable(R.styleable.Cards_imageSrc));

            a.recycle();
        }
    }

    // ------------------------------------------------------------------------
    // region API

    public void setTitle(@StringRes int value) {
        mTitle.setText(value);
    }

    public void setTitle(String value) {
        mTitle.setText(value);
    }

    public void setSubtitle(@StringRes int value) {
        mSubtitle.setText(value);
    }

    public void setSubtitle(String value) {
        mSubtitle.setText(value);
    }

    public void setImage(@DrawableRes int value) {
        mImage.setImageResource(value);
    }

    public void setImage(Drawable value) {
        mImage.setImageDrawable(value);
    }

    public void setImage(String url) {
        mImageHelper.load(url).withPalette(this).into(mImage);
    }

    public void setMainActionText(@StringRes int value) {
        mActionMain.setText(value);
    }

    public void setMainActionText(String value) {
        mActionMain.setText(value);
    }

    public void setMainActionVisibility(boolean visible) {
        if (visible)
            mActionMain.setVisibility(View.VISIBLE);
        else
            mActionMain.setVisibility(View.GONE);
    }

    public void setListener(final OnClickListener listener, final T object) {
        mListener = listener;

        mActionMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAddClicked(object);
            }
        });
        mActionLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onStartClicked(object);
            }
        });
        mActionRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onShowClicked(object);
            }
        });
    }
    // endregion
    // ------------------------------------------------------------------------

    public interface OnClickListener<T> {
        void onAddClicked(T object);

        void onStartClicked(T object);

        void onShowClicked(T object);
    }
}
