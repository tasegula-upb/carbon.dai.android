package ro.tasegula.carbon.android.common.controls;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import ro.tasegula.carbon.android.common.R;

public class RatingNumberControlsView extends RatingControlsView {

    public RatingNumberControlsView(Context context) {
        super(context);
    }

    public RatingNumberControlsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RatingNumberControlsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.view_controls_rating_numbers;
    }
}
